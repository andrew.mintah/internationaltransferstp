package com.ecobank.digx.cz.domain.payment.entity.payout.repository.adapter;


import com.ofss.digx.app.payment.dto.payout.InternationalPayoutResponseDomainDTO;
import com.ofss.digx.domain.payment.entity.PaymentKey;
import com.ofss.digx.domain.payment.entity.payout.InternationalPayout;
import com.ofss.digx.domain.payment.entity.payout.repository.adapter.IInternationalPayoutRepositoryAdapter;
import com.ofss.digx.domain.payment.entity.payout.repository.assembler.PaymentPayoutAssembler;
import com.ofss.digx.enumeration.payment.PaymentStatusType;
import com.ofss.digx.extxface.extxface.ExtxfaceAdapterFactory;
import com.ecobank.digx.cz.extxface.payment.adapter.IPaymentAdapter;
import com.ofss.digx.cz.eco.extxface.payment.mule.impl.PaymentAdapter;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.digx.framework.domain.repository.adapter.AbstractRemoteRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.app.context.SessionContext;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.enumeration.ServiceCallContextType;
import com.ofss.fc.infra.thread.ThreadAttribute;
import java.util.prefs.Preferences;
import java.util.List;

public class RemoteInternationalPayoutRepositoryAdapter extends AbstractRemoteRepositoryAdapter<InternationalPayout>
		implements
			IInternationalPayoutRepositoryAdapter {
	private static RemoteInternationalPayoutRepositoryAdapter instance;
	private Preferences digx_consulting = ConfigurationFactory.getInstance()
			.getConfigurations("DIGXCONSULTING");

	public static RemoteInternationalPayoutRepositoryAdapter getInstance() {
		if (instance == null) {
			Class var0 = RemoteInternationalPayoutRepositoryAdapter.class;
			synchronized (RemoteInternationalPayoutRepositoryAdapter.class) {
				if (instance == null) {
					instance = new RemoteInternationalPayoutRepositoryAdapter();
				}
			}
		}

		return instance;
	}

	public InternationalPayout read(PaymentKey key) throws Exception {
		InternationalPayout payout = new InternationalPayout();
		return payout;
	}

	public void create(InternationalPayout object) throws Exception {
		IPaymentAdapter adapter = (IPaymentAdapter) ExtxfaceAdapterFactory.getInstance().getAdapter(
				IPaymentAdapter.class, "processInternationalTransfer",
				DeterminantResolver.getInstance().getDeterminantTypeForObject(InternationalPayout.class.getName()));
		PaymentPayoutAssembler assembler = new PaymentPayoutAssembler();
		adapter.processInternationalTransfer(assembler.fromDomainObjectInternationalPayout(object));
	}

	public void update(InternationalPayout object) throws Exception {
	}

	public void delete(InternationalPayout object) throws Exception {
	}

	public List<InternationalPayout> list(String partyId, Date fromDate, Date toDate, PaymentStatusType status)
			throws Exception {
		return null;
	}

	/*public InternationalPayout process(InternationalPayout internationalPayout) throws Exception {
		SessionContext sessionContext = (SessionContext) ThreadAttribute.get("CTX");
		if (sessionContext.getServiceCallContextType() != null
				&& sessionContext.getServiceCallContextType() == ServiceCallContextType.VALIDATE) {
			return internationalPayout;
		} else {
			IPaymentAdapter adapter = (IPaymentAdapter) ExtxfaceAdapterFactory.getInstance().getAdapter(
					IPaymentAdapter.class, "processInternationalTransfer",
					DeterminantResolver.getInstance().getDeterminantTypeForObject(InternationalPayout.class.getName()));
			PaymentPayoutAssembler assembler = new PaymentPayoutAssembler();
			InternationalPayoutResponseDomainDTO response = adapter
					.processInternationalTransfer(assembler.fromDomainObjectInternationalPayout(internationalPayout));
			if (response != null) {
				internationalPayout.getTransactionReference().setExternalReferenceId(response.getHostReference());
				internationalPayout.setUniqueEndToEndTxnReference(response.getUniqueEndToEndTxnReference());
			}

			return internationalPayout;
		}
	}*/
	
	
		    
		

	

		public InternationalPayout process(InternationalPayout internationalPayout) throws Exception {
		SessionContext sessionContext = (SessionContext)ThreadAttribute.get("CTX");
		if (sessionContext.getServiceCallContextType() != null && sessionContext.getServiceCallContextType() == ServiceCallContextType.VALIDATE)
		return internationalPayout;
		IPaymentAdapter adapter = null;
		String excludedSwiftCodes = null, swiftForESB = null;
		excludedSwiftCodes = this.digx_consulting.get("Excluded_Swift_codes", "");
		swiftForESB = this.digx_consulting.get("Swift_for_ESB", "");
		Boolean isEcobankSwiftCode = Boolean.valueOf(false);
		if (internationalPayout.getSwiftCode() != null && internationalPayout.getSwiftCode() != "") {
		String swiftCode = internationalPayout.getSwiftCode();
		if (swiftForESB.contains(swiftCode.substring(0, 4)) && !excludedSwiftCodes.contains(swiftCode))
		isEcobankSwiftCode = Boolean.valueOf(true);
		}
		if (isEcobankSwiftCode.booleanValue()) {
		adapter = (IPaymentAdapter)ExtxfaceAdapterFactory.getInstance().getAdapter(IPaymentAdapter.class,
		"processInternationalSwiftTransfer",
		DeterminantResolver.getInstance().getDeterminantTypeForObject(InternationalPayout.class.getName()));
		System.out.println("Adapter Name: " + adapter);
		PaymentAdapter adapter1 = new PaymentAdapter();
		PaymentPayoutAssembler assembler = new PaymentPayoutAssembler();
		InternationalPayoutResponseDomainDTO response = adapter1.processInternationalTransfer(assembler.fromDomainObjectInternationalPayout(internationalPayout));
		System.out.println("Mule Response: " + response);
		internationalPayout.getTransactionReference().setExternalReferenceId(response.getHostReference());
		} else {
		adapter = (IPaymentAdapter)ExtxfaceAdapterFactory.getInstance().getAdapter(IPaymentAdapter.class, "processInternationalTransfer", DeterminantResolver.getInstance().getDeterminantTypeForObject(InternationalPayout.class.getName()));
		System.out.println("Adapter Name: " + adapter);
		PaymentPayoutAssembler assembler = new PaymentPayoutAssembler();
		InternationalPayoutResponseDomainDTO response = adapter.processInternationalTransfer(assembler.fromDomainObjectInternationalPayout(internationalPayout));
		internationalPayout.getTransactionReference().setExternalReferenceId(response.getHostReference());
		}
		return internationalPayout;
		}

		


	public List<InternationalPayout> lastPaymentList(Date filterDate) throws Exception {
		return null;
	}
}
