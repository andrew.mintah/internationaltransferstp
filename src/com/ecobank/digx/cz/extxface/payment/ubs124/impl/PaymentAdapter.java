package com.ecobank.digx.cz.extxface.payment.ubs124.impl;

import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.app.maintenance.dto.MaintenanceRequestDTO;
import com.ofss.digx.app.maintenance.dto.MaintenanceResponseDTO;
import com.ofss.digx.app.payment.dto.BillPaymentRequestDomainDTO;
import com.ofss.digx.app.payment.dto.BillPaymentResponseDomainDTO;
import com.ofss.digx.app.payment.dto.CancelInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.CancelInstructionResponseDomainDTO;
import com.ofss.digx.app.payment.dto.PaymentDetailsDTO;
import com.ofss.digx.app.payment.dto.ViewTransactionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.draft.DomesticDemandDraftRequestDomainDTO;
import com.ofss.digx.app.payment.dto.draft.DomesticDemandDraftResponseDomainDTO;
import com.ofss.digx.app.payment.dto.draft.InternationalDraftRequestDomainDTO;
import com.ofss.digx.app.payment.dto.draft.InternationalDraftResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.CancellationOfFutureDatedTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticPayoutInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticPayoutRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticPayoutResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCardPayoutRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCardPayoutResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCreditTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCreditTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaDirectDebitRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaDirectDebitResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUKPaymentsInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUKPaymentsRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUKPaymentsResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutCreateRequestDTO;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutCreateResponse;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.CancellationOfFutureDatedTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.DomesticSepaCardPayoutInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.DomesticSepaCreditTransferInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.DomesticSepaDirectDebitInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.PeerToPeerRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.PeerToPeerResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.externalTransfer.ExternalTransferDTO;
import com.ofss.digx.app.payment.dto.transfer.externalTransfer.ExternalTransferResponse;
import com.ofss.digx.enumeration.payment.InstructionType;
import com.ofss.digx.enumeration.payment.PaymentCancellationType;
import com.ofss.digx.enumeration.payment.PaymentType;
import com.ofss.digx.extxface.adapter.AbstractAdapter;
import com.ofss.digx.extxface.exceptions.ExtSystemTempUnavailableExeception;
import com.ecobank.digx.cz.extxface.payment.adapter.IPaymentAdapter;
import com.ofss.digx.extxface.payment.mock.impl.PaymentMockAdapter;
import com.ofss.digx.extxface.payment.ubs124.impl.draft.assembler.PaymentDraftAssembler;
import com.ofss.digx.extxface.payment.ubs124.impl.instructions.assembler.PaymentInstructionAssembler;
import com.ofss.digx.extxface.payment.ubs124.impl.instructions.payin.assembler.PaymentPayinInstructionAssembler;
import com.ofss.digx.extxface.payment.ubs124.impl.instructions.payout.assembler.PaymentPayoutInstructionAssembler;
import com.ofss.digx.extxface.payment.ubs124.impl.instructions.transfer.assembler.PaymentInstructionTransferAssembler;
//import com.ofss.digx.extxface.payment.ubs124.impl.payout.assembler.PaymentPayoutAssembler;
import com.ecobank.digx.cz.extxface.payment.ubs124.impl.transfer.assembler.PaymentTransferAssembler;
import com.ofss.digx.extxface.ubs124.impl.AbstractAdapterHelper;
import com.ofss.digx.extxface.ubs124.impl.PaymentAdapterHelper;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.infra.ws.JAXWSFactory;
import com.ofss.fc.infra.ws.WSException;
import com.ofss.fc.service.response.TransactionStatus;
import com.ofss.fcubs.gw.ws.types.fcubsifservice.FCUBSIFServiceSEI;
import com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSRES;
import com.ofss.fcubs124.gw.ws.types.fcubscustomerservice.FCUBSCustomerServiceSEI;
import com.ofss.fcubs124.gw.ws.types.fcubsddservice.FCUBSDDServiceSEI;
import com.ofss.fcubs124.gw.ws.types.fcubsextpcservice.FCUBSExtPCServiceSEI;
import com.ofss.fcubs124.gw.ws.types.fcubsftservice.FCUBSFTServiceSEI;
import com.ofss.fcubs124.gw.ws.types.fcubssiservice.FCUBSSIServiceSEI;
import com.ofss.fcubs124.gw.ws.types.fcubsupservice.FCUBSUPServiceSEI;
import com.ofss.fcubs124.service.fcubscustomerservice.CREATEAMTBLKFSFSRES;
import com.ofss.fcubs124.service.fcubsddservice.CREATETRANSACTIONFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.CLOSEPCMAINTFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.CREATEPCCONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.CREATEPCMAINTFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.REVERSEPCCONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsftservice.CLOSECONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsftservice.CREATECONTRACTFSFSRES;
//import com.ofss.fcubs124.service.fcubssiservice.CLOSECONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsupservice.CREATEUPTRANSACTIONFSFSRES;
import com.ecobank.digx.cz.extxface.payment.ubs124.impl.PaymentAdapterResponseHandler;
import com.ecobank.digx.cz.extxface.payment.ubs124.impl.payout.assembler.PaymentPayoutAssembler;

import java.net.SocketTimeoutException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.ws.WebServiceException;

public class PaymentAdapter extends AbstractAdapter implements IPaymentAdapter {
  private static final String THIS_COMPONENT_NAME = PaymentAdapter.class.getName();
  
  private static final MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
  
  private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
  
  private final PaymentAdapterResponseHandler responseHandler = new PaymentAdapterResponseHandler();
  
  public SelfTransferResponseDomainDTO processSelfTransfer(SelfTransferRequestDomainDTO selfTransferReqDTO) throws Exception {
    if (logger.isLoggable(Level.FINE))
      logger.log(Level.FINE, formatter
          .formatMessage("Entered in method processSelfTransfer of %s used for self transfer, %s", new Object[] { THIS_COMPONENT_NAME, selfTransferReqDTO })); 
    checkRequest("com.ofss.digx.extxface.payment.ubs124.impl.PaymentAdapter.processSelfTransfer", new Object[] { selfTransferReqDTO });
    if (isMockingEnabled(PaymentType.SELFFT)) {
      PaymentMockAdapter mockAdapter = new PaymentMockAdapter();
      SelfTransferResponseDomainDTO selfTransferResponseDomainDTO = null;
      selfTransferResponseDomainDTO = mockAdapter.processSelfTransfer(selfTransferReqDTO);
      return selfTransferResponseDomainDTO;
    } 
    AdapterInteraction.begin();
    SelfTransferResponseDomainDTO selfTransferResponse = null;
    PaymentTransferAssembler paymentTransferAssembler = new PaymentTransferAssembler();
    CREATEEXTACCECAENTRIESFSFSRES createContractIFFS = null;
    try {
    	FCUBSIFServiceSEI clientProcess = (FCUBSIFServiceSEI)JAXWSFactory.createServiceStub("FCUBSIFServiceSEI", "createExtAccEcaEntriesFS");
      createContractIFFS = clientProcess.createExtAccEcaEntriesFS(paymentTransferAssembler.fromAdaptertoHostRequestSelf(selfTransferReqDTO));
    } catch (WSException e) {
      logger.log(Level.SEVERE, formatter.formatMessage(" Exception has occured while getting response object of %s inside the createPCContractFS method of %s for %s. Exception details are %s", new Object[] { SelfTransferRequestDomainDTO.class
              
              .getName(), THIS_COMPONENT_NAME, selfTransferReqDTO, e }));
    } finally {
      AdapterInteraction.close();
    } 
    this.responseHandler.checkResponseProcessSelfTransfer(createContractIFFS);
    selfTransferResponse = paymentTransferAssembler.fromHosttoAdapterResponseSelf(createContractIFFS);
    setExternalReferenceNumber(selfTransferResponse.getHostReference());
    checkResponse(selfTransferResponse);
    if (logger.isLoggable(Level.FINE))
      logger.log(Level.FINE, formatter.formatMessage("Exiting from method processSelfTransfer of %s used to process self transfer, selfTransferReqDTO  = %s", new Object[] { THIS_COMPONENT_NAME, selfTransferReqDTO })); 
    return selfTransferResponse;
  }
  
  
  public SelfTransferResponseDomainDTO processSelfTransferInstruction(SelfTransferInstructionRequestDomainDTO selfTransferInstructionRequest) throws Exception {
    if (logger.isLoggable(Level.FINE))
      logger.log(Level.FINE, formatter
          .formatMessage("Entered in method processSelfTransferInstruction of %s used for self transfer, %s", new Object[] { THIS_COMPONENT_NAME, selfTransferInstructionRequest })); 
    checkRequest("com.ofss.digx.extxface.payment.ubs124.impl.PaymentAdapter.processSelfTransferInstruction", new Object[] { selfTransferInstructionRequest });
    if (selfTransferInstructionRequest.getInstructionDetails().getType().equals(InstructionType.NONRECURRING))
      return processSelfTransfer(selfTransferInstructionRequest.getPaymentDetails()); 
    if (isMockingEnabled(PaymentType.SELFFT_SI)) {
      PaymentMockAdapter mockAdapter = new PaymentMockAdapter();
      SelfTransferResponseDomainDTO selfTransferResponse = null;
      selfTransferResponse = mockAdapter.processSelfTransferInstruction(selfTransferInstructionRequest);
      return selfTransferResponse;
    } 
    AdapterInteraction.begin();
    SelfTransferResponseDomainDTO responseDTO = null;
    PaymentInstructionTransferAssembler paymentTransferAssembler = new PaymentInstructionTransferAssembler();
    CREATEPCMAINTFSFSRES createContractPCFS = null;
    try {
      FCUBSExtPCServiceSEI clientProcess = (FCUBSExtPCServiceSEI)JAXWSFactory.createServiceStub("FCUBSExtPCServiceSEI", "createPCContract");
      createContractPCFS = clientProcess.createPCMaintFS(paymentTransferAssembler
          .fromAdaptertoHostRequestSelfTransferSI(selfTransferInstructionRequest));
    } catch (WSException e) {
      logger.log(Level.SEVERE, formatter.formatMessage(" Exception has occured while getting response object of %s inside the createPCContractFS method of %s for %s. Exception details are %s", new Object[] { PeerToPeerRequestDomainDTO.class
              
              .getName(), THIS_COMPONENT_NAME, selfTransferInstructionRequest, e }));
    } finally {
      AdapterInteraction.close();
    } 
    this.responseHandler.checkResponseProcessSelfTransferSI(createContractPCFS);
    responseDTO = paymentTransferAssembler.fromHosttoAdapterResponseSelfTransferSI(createContractPCFS);
    setExternalReferenceNumber(responseDTO.getHostReference());
    checkResponse(responseDTO);
    if (logger.isLoggable(Level.FINE))
      logger.log(Level.FINE, formatter.formatMessage("Exiting from method processSelfTransferInstruction of %s used to process self transfer, selfTransferInstructionRequest  = %s", new Object[] { THIS_COMPONENT_NAME, selfTransferInstructionRequest })); 
    return responseDTO;
  }
  
  public InternalTransferResponseDomainDTO processInternalAccountTransferInstruction(InternalTransferInstructionRequestDomainDTO internalTransferInstructionRequest) throws Exception {
    if (logger.isLoggable(Level.FINE))
      logger.log(Level.FINE, formatter.formatMessage("Entered in method processInternalAccountTransferInstruction of %s used for internal transfer instruction, %s", new Object[] { THIS_COMPONENT_NAME, internalTransferInstructionRequest })); 
    checkRequest("com.ofss.digx.extxface.payment.ubs124.impl.PaymentAdapter.processInternalAccountTransferInstruction", new Object[] { internalTransferInstructionRequest });
    if (internalTransferInstructionRequest.getInstructionDetails().getType().equals(InstructionType.NONRECURRING))
      return processInternalTransfer(internalTransferInstructionRequest.getPaymentDetails()); 
    if (isMockingEnabled(PaymentType.INTERNALFT_SI)) {
      PaymentMockAdapter mockAdapter = new PaymentMockAdapter();
      InternalTransferResponseDomainDTO internalTransferResponse = null;
      internalTransferResponse = mockAdapter.processInternalAccountTransferInstruction(internalTransferInstructionRequest);
      return internalTransferResponse;
    } 
    AdapterInteraction.begin();
    InternalTransferResponseDomainDTO responseDTO = null;
    PaymentInstructionTransferAssembler paymentTransferAssembler = new PaymentInstructionTransferAssembler();
    CREATEPCMAINTFSFSRES createContractPCFS = null;
    try {
      FCUBSExtPCServiceSEI clientProcess = (FCUBSExtPCServiceSEI)JAXWSFactory.createServiceStub("FCUBSExtPCServiceSEI", "createPCContract");
      createContractPCFS = clientProcess.createPCMaintFS(paymentTransferAssembler
          .fromAdaptertoHostRequestInternalTransferSI(internalTransferInstructionRequest));
    } catch (WSException e) {
      logger.log(Level.SEVERE, formatter.formatMessage(" Exception has occured while getting response object of %s inside the createPCContractFS method of %s for %s. Exception details are %s", new Object[] { PeerToPeerRequestDomainDTO.class
              
              .getName(), THIS_COMPONENT_NAME, internalTransferInstructionRequest, e }));
    } finally {
      AdapterInteraction.close();
    } 
    this.responseHandler.checkResponseProcessInternalTransferSI(createContractPCFS);
    responseDTO = paymentTransferAssembler.fromHosttoAdapterResponseInternalTransferSI(createContractPCFS);
    setExternalReferenceNumber(responseDTO.getHostReference());
    checkResponse(responseDTO);
    if (logger.isLoggable(Level.FINE))
      logger.log(Level.FINE, formatter.formatMessage("Exiting from method processInternalAccountTransferInstruction of %s used to process internal transfer instruction, internalTransferInstructionRequest  = %s", new Object[] { THIS_COMPONENT_NAME, internalTransferInstructionRequest })); 
    return responseDTO;
  }
  
  public InternalTransferResponseDomainDTO processInternalTransfer(InternalTransferRequestDomainDTO internalTransferReqDTO) throws Exception {
    if (logger.isLoggable(Level.FINE))
      logger.log(Level.FINE, formatter
          .formatMessage("Entered in method processInternalTransfer of %s used for internal transfer, %s", new Object[] { THIS_COMPONENT_NAME, internalTransferReqDTO })); 
    checkRequest("com.ofss.digx.extxface.payment.ubs124.impl.PaymentAdapter.processInternalTransfer", new Object[] { internalTransferReqDTO });
    if (isMockingEnabled(PaymentType.INTERNALFT)) {
      PaymentMockAdapter mockAdapter = new PaymentMockAdapter();
      InternalTransferResponseDomainDTO internalTransferResponseDomainDTO = null;
      internalTransferResponseDomainDTO = mockAdapter.processInternalTransfer(internalTransferReqDTO);
      return internalTransferResponseDomainDTO;
    } 
    AdapterInteraction.begin();
    InternalTransferResponseDomainDTO internalTransferResponse = null;
    PaymentTransferAssembler paymentTransferAssembler = new PaymentTransferAssembler();
    CREATEPCCONTRACTFSFSRES createContractPCResp = null;
    try {
      FCUBSExtPCServiceSEI clientProcess = (FCUBSExtPCServiceSEI)JAXWSFactory.createServiceStub("FCUBSExtPCServiceSEI", "createPCContract");
      createContractPCResp = clientProcess.createPCContractFS(paymentTransferAssembler
          .fromAdaptertoHostRequestInternal(internalTransferReqDTO));
    } catch (WSException e) {
      logger.log(Level.SEVERE, formatter.formatMessage(" Exception has occured while getting response object of %s inside the createPCContract method of %s for %s. Exception details are %s", new Object[] { InternalTransferRequestDomainDTO.class
              
              .getName(), THIS_COMPONENT_NAME, internalTransferReqDTO, e }));
    } finally {
      AdapterInteraction.close();
    } 
    this.responseHandler.checkResponseProcessInternalTransfer(createContractPCResp);
    internalTransferResponse = paymentTransferAssembler.fromHosttoAdapterResponseInternal(createContractPCResp);
    setExternalReferenceNumber(internalTransferResponse.getHostReference());
    checkResponse(internalTransferResponse);
    if (logger.isLoggable(Level.FINE))
      logger.log(Level.FINE, formatter.formatMessage("Exiting from method processInternalTransfer of %s used to process internal transfer, internalTransferReqDTO  = %s", new Object[] { THIS_COMPONENT_NAME, internalTransferReqDTO })); 
    return internalTransferResponse;
  }
  
  public InternationalPayoutResponseDomainDTO processInternationalTransfer(
			InternationalPayoutRequestDomainDTO internationalPayoutRequestDTO) throws Exception {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, formatter.formatMessage(
					"Entered in method processInternationalTransfer of %s used for international payments SI, %s",
					new Object[]{THIS_COMPONENT_NAME, internationalPayoutRequestDTO}));
		}

		super.checkRequest("com.ofss.digx.extxface.payment.ubs124.impl.PaymentAdapter.processInternationalTransfer",
				new Object[]{internationalPayoutRequestDTO});
		AdapterInteraction.begin();
		InternationalPayoutResponseDomainDTO internationalPayoutResponse;
		if (this.isMockingEnabled(PaymentType.INTERNATIONALFT)) {
			PaymentMockAdapter mockAdapter = new PaymentMockAdapter();
			internationalPayoutResponse = null;
			internationalPayoutResponse = mockAdapter.processInternationalTransfer(internationalPayoutRequestDTO);
			return internationalPayoutResponse;
		} else {
			internationalPayoutResponse = null;
			PaymentPayoutAssembler paymentPayoutAssembler = new PaymentPayoutAssembler();
			CREATECONTRACTFSFSRES createContractFSResp = null;

			try {
				FCUBSFTServiceSEI clientProcess = (FCUBSFTServiceSEI) JAXWSFactory
						.createServiceStub("FCUBSFTServiceSEI", "createFTService");
				createContractFSResp = clientProcess.createContractFS(paymentPayoutAssembler
						.fromAdaptertoHostRequestInternationalPayout(internationalPayoutRequestDTO));
				this.responseHandler.checkResponseProcessInternationalPayout(createContractFSResp);
				internationalPayoutResponse = paymentPayoutAssembler
						.fromHosttoAdapterResponseInternationalPayout(createContractFSResp);
				this.setExternalReferenceNumber(internationalPayoutResponse.getHostReference());
				System.out.println("Inside Adapter Call Andrew");
			} catch (WSException var11) {
				logger.log(Level.SEVERE, formatter.formatMessage(
						" Exception has occured while getting response object of %s inside the createPCContract method of %s for %s. Exception details are %s",
						new Object[]{InternationalPayoutRequestDomainDTO.class.getName(), THIS_COMPONENT_NAME,
								internationalPayoutRequestDTO}),
						var11);
				this.responseHandler.translateAndThrow(var11, this.getClass());
			} catch (WebServiceException var12) {
				logger.log(Level.SEVERE, formatter.formatMessage(
						" WebServiceException has occured while getting response object of %s inside the createPCContract method of %s for %s. Exception details are %s",
						new Object[]{InternationalPayoutRequestDomainDTO.class.getName(), THIS_COMPONENT_NAME,
								internationalPayoutRequestDTO}),
						var12);
				if (var12.getCause() != null && var12.getCause() instanceof SocketTimeoutException) {
					throw new ExtSystemTempUnavailableExeception();
				}

				throw var12;
			} finally {
				AdapterInteraction.close();
			}

			super.checkResponse(internationalPayoutResponse);
			if (logger.isLoggable(Level.FINE)) {
				logger.log(Level.FINE, formatter.formatMessage(
						"Exiting from method processSepaCardPayoutInstruction of %s used to process international payments SI, internationalPayoutRequestDTO  = %s",
						new Object[]{THIS_COMPONENT_NAME, internationalPayoutRequestDTO}));
			}

			return internationalPayoutResponse;
		}
	}
	

  
  
  
  
  
  
  
  
  private boolean isMockingEnabled(PaymentType internalftSi) {
    String mockFlag;
    switch (internalftSi) {
      case SELFFT_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("SELFFT_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case INTERNALFT_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("INTERNALFT_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case INTERNATIONALFT_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("INTERNATIONALFT_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case INDIADOMESTICFT_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("INDIADOMESTICFT_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case UKPAYMENTS_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("UKPAYMENTS_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case SEPADIRECTDEBIT_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("SEPADIRECTDEBIT_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case SEPACREDITTRANSFER_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("SEPACREDITTRANSFER_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case SEPACARDPAYMENT_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("SEPACARDPAYMENT_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case INTERNATIONALDRAFT_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("INTERNATIONALDRAFT_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case DOMESTICDRAFT_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("DOMESTICDRAFT_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case PEER_TO_PEER_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("PEER_TO_PEER_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case BILLPAYMENT_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("BILLPAYMENT_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
      case DOMESTICFT_PAYLATER:
        mockFlag = AbstractAdapterHelper.getInstance().getAdapterImplConfiguration("DOMESTICFT_CANCELLATION", "MOCKED");
        if (mockFlag != null && mockFlag.equalsIgnoreCase("Y"))
          return true; 
        break;
    } 
    return false;
  }
  
  public PeerToPeerResponseDomainDTO processCreditPeerToPeerTransfer(PeerToPeerRequestDomainDTO p2pTransferDTO) throws Exception {
    return null;
  }
  
  
  public PeerToPeerResponseDomainDTO processRemoveEarmarkAndDebitPeerToPeerTransfer(PeerToPeerRequestDomainDTO domainDTO) throws Exception {
    return null;
  }
  
  public TransactionStatus updateSelfTransferInstruction(SelfTransferInstructionRequestDomainDTO selfTransferInstructionRequest) throws Exception {
    return null;
  }




}
