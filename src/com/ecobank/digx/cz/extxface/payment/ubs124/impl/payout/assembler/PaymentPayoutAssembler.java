package com.ecobank.digx.cz.extxface.payment.ubs124.impl.payout.assembler;

import java.net.SocketTimeoutException;
import java.util.logging.Level;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.ws.WebServiceException;

import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutResponseDomainDTO;
import com.ofss.digx.enumeration.payment.PaymentType;
import com.ofss.digx.extxface.exceptions.ExtSystemTempUnavailableExeception;
import com.ofss.digx.extxface.payment.mock.impl.PaymentMockAdapter;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.ws.JAXWSFactory;
import com.ofss.fc.infra.ws.WSException;
import com.ofss.fcubs124.gw.ws.types.fcubsftservice.FCUBSFTServiceSEI;
import com.ofss.fcubs124.service.fcubsftservice.CREATECONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsftservice.ContractFullType;
import com.ofss.fcubs124.service.fcubsftservice.TxnSetlDtlsFullType;
import com.ofss.digx.app.maintenance.dto.MaintenanceRequestDTO;
import com.ofss.digx.app.maintenance.dto.MaintenanceResponseDTO;
import com.ofss.digx.app.payment.dto.BillPaymentRequestDomainDTO;
import com.ofss.digx.app.payment.dto.BillPaymentResponseDomainDTO;
import com.ofss.digx.app.payment.dto.CancelInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.CancelInstructionResponseDomainDTO;
import com.ofss.digx.app.payment.dto.PaymentDetailsDTO;
import com.ofss.digx.app.payment.dto.ViewTransactionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.draft.DomesticDemandDraftRequestDomainDTO;
import com.ofss.digx.app.payment.dto.draft.DomesticDemandDraftResponseDomainDTO;
import com.ofss.digx.app.payment.dto.draft.InternationalDraftRequestDomainDTO;
import com.ofss.digx.app.payment.dto.draft.InternationalDraftResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.CancellationOfFutureDatedTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticPayoutInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticPayoutRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticPayoutResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCardPayoutRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCardPayoutResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCreditTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCreditTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaDirectDebitRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaDirectDebitResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUKPaymentsInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUKPaymentsRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUKPaymentsResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutCreateRequestDTO;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutCreateResponse;
import com.ofss.digx.app.payment.dto.transfer.CancellationOfFutureDatedTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.DomesticSepaCardPayoutInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.DomesticSepaCreditTransferInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.DomesticSepaDirectDebitInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.PeerToPeerRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.PeerToPeerResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.externalTransfer.ExternalTransferDTO;
import com.ofss.digx.app.payment.dto.transfer.externalTransfer.ExternalTransferResponse;
import com.ofss.digx.enumeration.payment.InstructionType;
import com.ofss.digx.enumeration.payment.PaymentCancellationType;
import com.ofss.digx.extxface.adapter.AbstractAdapter;
import com.ecobank.digx.cz.extxface.payment.adapter.IPaymentAdapter;
import com.ofss.digx.extxface.payment.ubs124.impl.draft.assembler.PaymentDraftAssembler;
import com.ofss.digx.extxface.payment.ubs124.impl.instructions.assembler.PaymentInstructionAssembler;
import com.ofss.digx.extxface.payment.ubs124.impl.instructions.payin.assembler.PaymentPayinInstructionAssembler;
import com.ofss.digx.extxface.payment.ubs124.impl.instructions.payout.assembler.PaymentPayoutInstructionAssembler;
import com.ofss.digx.extxface.payment.ubs124.impl.instructions.transfer.assembler.PaymentInstructionTransferAssembler;
import com.ecobank.digx.cz.extxface.payment.ubs124.impl.transfer.assembler.PaymentTransferAssembler;
import com.ofss.digx.extxface.ubs124.impl.AbstractAdapterHelper;
import com.ofss.digx.extxface.ubs124.impl.PaymentAdapterHelper;
import com.ofss.digx.extxface.ubs124.impl.RequestHeader;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.service.response.TransactionStatus;
import com.ofss.fcubs.gw.ws.types.fcubsifservice.FCUBSIFServiceSEI;
import com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSRES;
import com.ofss.fcubs124.gw.ws.types.fcubscustomerservice.FCUBSCustomerServiceSEI;
import com.ofss.fcubs124.gw.ws.types.fcubsddservice.FCUBSDDServiceSEI;
import com.ofss.fcubs124.gw.ws.types.fcubsextpcservice.FCUBSExtPCServiceSEI;
import com.ofss.fcubs124.gw.ws.types.fcubssiservice.FCUBSSIServiceSEI;
import com.ofss.fcubs124.gw.ws.types.fcubsupservice.FCUBSUPServiceSEI;
import com.ofss.fcubs124.service.fcubscustomerservice.CREATEAMTBLKFSFSRES;
import com.ofss.fcubs124.service.fcubsddservice.CREATETRANSACTIONFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.CLOSEPCMAINTFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.CREATEPCCONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.CREATEPCMAINTFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.REVERSEPCCONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsftservice.CLOSECONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsftservice.CREATECONTRACTFSFSREQ;
//import com.ofss.fcubs124.service.fcubssiservice.CLOSECONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsupservice.CREATEUPTRANSACTIONFSFSRES;
import com.ecobank.digx.cz.extxface.payment.ubs124.impl.PaymentAdapter;
import com.ecobank.digx.cz.extxface.payment.ubs124.impl.PaymentAdapterResponseHandler;

import java.util.List;
import java.util.logging.Logger;

public class PaymentPayoutAssembler {
	 private static final String THIS_COMPONENT_NAME = PaymentAdapter.class.getName();
	 private static final MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	  
	  private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	  
	  private final PaymentAdapterResponseHandler responseHandler = new PaymentAdapterResponseHandler();
	  
	
	  public CREATECONTRACTFSFSREQ fromAdaptertoHostRequestInternationalPayout(
				InternationalPayoutRequestDomainDTO internationalPayoutRequestDTO) throws Exception {
			AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
			PaymentAdapterHelper paymentAdapterHelper = PaymentAdapterHelper.getInstance();
			RequestHeader header = helper.getRequestHeader((String) null, "FCUBSFTService", "CreateContract");
			CREATECONTRACTFSFSREQ createContractFSReq = new CREATECONTRACTFSFSREQ();
			//TxnSetlDtlsFullType settlements = new TxnSetlDtlsFullType();
			//ContractSettlement[] contractSettlement = new ContractSettlement[2];
			
			//contractSettlement[0] = new ContractSettlement();
			//contractSettlement[1] = new ContractSettlement();
			
		    TxnSetlDtlsFullType settlements = new TxnSetlDtlsFullType();
		    TxnSetlDtlsFullType.ContractSettlement[] contractSettlement = new TxnSetlDtlsFullType.ContractSettlement[2];
		    contractSettlement[0] = new TxnSetlDtlsFullType.ContractSettlement();
		    contractSettlement[1] = new TxnSetlDtlsFullType.ContractSettlement();


			com.ofss.fcubs124.service.fcubsftservice.FCUBSHEADERType fcUBSHeaderType = new com.ofss.fcubs124.service.fcubsftservice.FCUBSHEADERType();
			fcUBSHeaderType.setUSERID(header.getUserid());
			fcUBSHeaderType.setCORRELID(internationalPayoutRequestDTO.getSystemReferenceNumber());
			fcUBSHeaderType.setMSGID(internationalPayoutRequestDTO.getSystemReferenceNumber());
			fcUBSHeaderType.setBRANCH(helper.getProcessingBranch() != null
					? helper.getProcessingBranch()
					: helper.getHostBranchId(internationalPayoutRequestDTO.getDebitAccountId()));
			fcUBSHeaderType.setUBSCOMP(com.ofss.fcubs124.service.fcubsftservice.UBSCOMPType.FCUBS);
			fcUBSHeaderType.setSERVICE(header.getService());
			fcUBSHeaderType.setOPERATION(header.getOperation());
			fcUBSHeaderType.setSOURCE("FCATHOLD");
			createContractFSReq.setFCUBSHEADER(fcUBSHeaderType);
			com.ofss.fcubs124.service.fcubsftservice.CREATECONTRACTFSFSREQ.FCUBSBODY fcUBSBody = new com.ofss.fcubs124.service.fcubsftservice.CREATECONTRACTFSFSREQ.FCUBSBODY();
			ContractFullType contractFullType = new ContractFullType();
			contractFullType.setSOURCEREFNO(internationalPayoutRequestDTO.getSystemReferenceNumber());
			contractFullType.setBYORDEROF1(internationalPayoutRequestDTO.getPartyId());
			contractFullType.setDBTBRN(helper.getHostBranchId(internationalPayoutRequestDTO.getDebitAccountId()));
			contractFullType.setDBTACC(helper.getHostAccountId(internationalPayoutRequestDTO.getDebitAccountId()));
			contractFullType.setDRCCY(paymentAdapterHelper.fetchAccountCurrency(
					helper.getHostAccountId(internationalPayoutRequestDTO.getDebitAccountId()),
					helper.getHostBranchId(internationalPayoutRequestDTO.getDebitAccountId())));
			if (internationalPayoutRequestDTO.getDealId() != null) {
				contractFullType.setRATEREF(internationalPayoutRequestDTO.getDealId());
				contractFullType.setPROD(helper.getAdapterImplConfiguration("INTERNATIONALFT_FX", "PRODCOD"));
			} else {
				contractFullType.setPROD(helper.getAdapterImplConfiguration("INTERNATIONALFT", "PRODCOD"));
			}

			if (internationalPayoutRequestDTO.getPmtAmount() != null) {
				contractFullType.setCRAMT(internationalPayoutRequestDTO.getPmtAmount().getAmount());
				contractFullType.setCRCCY(internationalPayoutRequestDTO.getPmtAmount().getCurrency());
			}

			contractFullType.setCRACC("");

			try {
				if (internationalPayoutRequestDTO.getPaymentDate() != null) {
					contractFullType.setDRVALDT(helper.getPaymentHostDate(paymentAdapterHelper.fetchNextWorkingDate(
							helper.getHostBranchId(internationalPayoutRequestDTO.getDebitAccountId()),
							internationalPayoutRequestDTO.getPaymentDate())));
					contractFullType.setCRVALDT(helper.getPaymentHostDate(paymentAdapterHelper.fetchNextWorkingDate(
							helper.getHostBranchId(internationalPayoutRequestDTO.getDebitAccountId()),
							internationalPayoutRequestDTO.getPaymentDate())));
				} else {
					contractFullType.setDRVALDT(helper.getPaymentHostDate(paymentAdapterHelper
							.fetchValueDate(helper.getHostBranchId(internationalPayoutRequestDTO.getDebitAccountId()))));
					contractFullType.setCRVALDT(helper.getPaymentHostDate(paymentAdapterHelper
							.fetchValueDate(helper.getHostBranchId(internationalPayoutRequestDTO.getDebitAccountId()))));
				}
			} catch (DatatypeConfigurationException var12) {
				logger.log(Level.SEVERE, formatter.formatMessage(
						" Exception has occured while getting response object of %s inside the fromAdaptertoHostRequestInternationalPayout method of %s for %s. Exception details are %s",
						new Object[]{InternationalPayoutRequestDomainDTO.class.getName(), THIS_COMPONENT_NAME,
								internationalPayoutRequestDTO, var12}),
						var12);
			}

			if (internationalPayoutRequestDTO.getBeneficiary() != null
					&& internationalPayoutRequestDTO.getBeneficiary()[0] != null
					&& internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails() != null
					&& internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0] != null) {
				contractFullType
						.setCHRGBEARER(internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
								.getCorrespondanceCodes());
			}

			if (internationalPayoutRequestDTO.getPaymentDeatils() != null
					&& internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails1() != null
					&& internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails1() != "") {
				contractFullType.setPAYMENTDET1(internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails1());
			} else {
				contractFullType.setPAYMENTDET1("");
			}

			if (internationalPayoutRequestDTO.getPaymentDeatils() != null
					&& internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails2() != null
					&& internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails2() != "") {
				contractFullType.setPAYMENTDET2(internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails2());
			} else {
				contractFullType.setPAYMENTDET2("");
			}

			if (internationalPayoutRequestDTO.getPaymentDeatils() != null
					&& internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails3() != null
					&& internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails3() != "") {
				contractFullType.setPAYMENTDET3(internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails3());
			} else {
				contractFullType.setPAYMENTDET3("");
			}

			if (internationalPayoutRequestDTO.getPaymentDeatils() != null
					&& internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails4() != null
					&& internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails4() != "") {
				contractFullType.setPAYMENTDET4(internationalPayoutRequestDTO.getPaymentDeatils().getPayDetails4());
			} else {
				contractFullType.setPAYMENTDET4("");
			}

			if (helper.getHostAccountId(internationalPayoutRequestDTO.getCreditAccountId()) != null
					&& helper.getHostAccountId(internationalPayoutRequestDTO.getCreditAccountId()) != "") {
				contractFullType.setULTBEN1(internationalPayoutRequestDTO.getCreditAccountId());
			} else if (internationalPayoutRequestDTO.getBeneficiary() != null
					&& internationalPayoutRequestDTO.getBeneficiary()[0] != null) {
				contractFullType.setULTBEN1(internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryName());
			}

			if (internationalPayoutRequestDTO.getBeneficiary() != null
					&& internationalPayoutRequestDTO.getBeneficiary()[0] != null) {
				contractFullType.setULTBEN2(internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryName());
				contractFullType.setSTULTBEN(internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryName());
				if (internationalPayoutRequestDTO.getBeneficiary()[0].getAddressDTO() != null
						&& internationalPayoutRequestDTO.getBeneficiary()[0].getAddressDTO()[0] != null) {
					if (internationalPayoutRequestDTO.getBeneficiary()[0].getAddressDTO()[0].getLine1() != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getAddressDTO()[0].getLine1() != "") {
						contractFullType.setULTBEN3(
								internationalPayoutRequestDTO.getBeneficiary()[0].getAddressDTO()[0].getLine1());
					}

					if (internationalPayoutRequestDTO.getBeneficiary()[0].getAddressDTO()[0].getLine2() != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getAddressDTO()[0].getLine2() != "") {
						contractFullType.setULTBEN4(
								internationalPayoutRequestDTO.getBeneficiary()[0].getAddressDTO()[0].getLine2());
					}

					contractFullType
							.setULTBEN5(internationalPayoutRequestDTO.getBeneficiary()[0].getAddressDTO()[0].getCity());
					contractFullType
							.setULTIBEN6(internationalPayoutRequestDTO.getBeneficiary()[0].getAddressDTO()[0].getCountry());
				}

				if (internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails() != null
						&& internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0] != null) {
					if (internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
							.getNationalClringCod() != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
									.getNationalClringCod() != "") {
						contractFullType
								.setAWI(internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
										.getNationalClringCod());
					} else if (internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
							.getSwiftCode() != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
									.getSwiftCode() != "") {
						contractFullType
								.setAWI(internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
										.getSwiftCode());
					} else if (internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
							.getBeneBankBranchDTO() != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
									.getBeneBankBranchDTO()[0] != null) {
						contractFullType
								.setAWI(internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
										.getBeneBankBranchDTO()[0].getNamBank());
					}

					if (internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
							.getAddressDTO() != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
									.getAddressDTO()[0] != null) {
						contractFullType
								.setAWI2(internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
										.getAddressDTO()[0].getLine1());
						contractFullType
								.setAWI3(internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
										.getAddressDTO()[0].getCity());
						contractFullType
								.setAWI6(internationalPayoutRequestDTO.getBeneficiary()[0].getBeneficiaryBankDetails()[0]
										.getAddressDTO()[0].getCountry());
					}
				}

				contractFullType.setINTRMKS(internationalPayoutRequestDTO.getRemarks());
				contractFullType.setAUTHSTAT("U");
				if (internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails() != null
						&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0] != null) {
					 contractSettlement[0].setCOMPNT("AMT_EQUIV");
					contractSettlement[1].setCOMPNT("TFR_AMT");
					if (internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
							.getSwiftCode() != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getSwiftCode() != "") {
						contractFullType.setSTINTRMED(
								internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
										.getSwiftCode());
						contractSettlement[0].setINTERMEDIARY1(
								internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
										.getSwiftCode());
						contractSettlement[1].setINTERMEDIARY1(
								internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
										.getSwiftCode());
					} else if (internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
							.getNationalClringCod() != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getNationalClringCod() != "") {
						contractFullType.setSTINTRMED(
								internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
										.getNationalClringCod());
						contractSettlement[0].setINTERMEDIARY1(
								internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
										.getNationalClringCod());
						contractSettlement[1].setINTERMEDIARY1(
								internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
										.getNationalClringCod());
					} else {
						contractFullType.setSTINTRMED("");
						contractSettlement[0].setINTERMEDIARY1("");
						contractSettlement[1].setINTERMEDIARY1("");
					}

					if (internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
							.getBeneBankBranchDTO() != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getBeneBankBranchDTO()[0] != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getBeneBankBranchDTO()[0].getNamBank() != null
							&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getBeneBankBranchDTO()[0].getNamBank() != ""
							|| internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getAddressDTO() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0] != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getLine1() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getLine1() != ""
							|| internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getAddressDTO() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0] != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getLine2() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getLine2() != ""
							|| internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getAddressDTO() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0] != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getCity() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getCity() != ""
							|| internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getAddressDTO() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0] != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getCountry() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getCountry() != "") {
						if (internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
								.getBeneBankBranchDTO() != null
								&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
										.getBeneBankBranchDTO()[0] != null
								&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
										.getBeneBankBranchDTO()[0].getNamBank() != null
								&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
										.getBeneBankBranchDTO()[0].getNamBank() != "") {
							contractSettlement[0].setINTERMEDIARY1(
									internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getBeneBankBranchDTO()[0].getNamBank());
						} else {
							contractSettlement[0].setINTERMEDIARY1("");
						}

						if (internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
								.getAddressDTO() != null
								&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
										.getAddressDTO()[0] != null) {
							if (internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getAddressDTO()[0].getLine1() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getLine1() != "") {
								contractSettlement[0].setINTERMEDIARY2(
										internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
												.getAddressDTO()[0].getLine1());
							} else {
								contractSettlement[0].setINTERMEDIARY2("");
							}

							if (internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getAddressDTO()[0].getLine2() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getLine2() != "") {
								contractSettlement[0].setINTERMEDIARY3(
										internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
												.getAddressDTO()[0].getLine2());
							} else {
								contractSettlement[0].setINTERMEDIARY3("");
							}

							if (internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getAddressDTO()[0].getCity() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getCity() != "") {
								contractSettlement[0].setINTERMEDIARY4(
										internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
												.getAddressDTO()[0].getCity());
							} else {
								contractSettlement[0].setINTERMEDIARY4("");
							}

							if (internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
									.getAddressDTO()[0].getCountry() != null
									&& internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
											.getAddressDTO()[0].getCountry() != "") {
								contractSettlement[0].setINTERMEDIARY5(
										internationalPayoutRequestDTO.getBeneficiary()[0].getIntermediaryBankDetails()[0]
												.getAddressDTO()[0].getCountry());
							} else {
								contractSettlement[0].setINTERMEDIARY5("");
							}
						}
					}
				}
			}

			settlements.setContractSettlement(contractSettlement);
			contractFullType.setSettlements(settlements);
			fcUBSBody.setContractDetailsFull(contractFullType);
			createContractFSReq.setFCUBSBODY(fcUBSBody);
			return createContractFSReq;
		}
	  
		public InternationalPayoutResponseDomainDTO fromHosttoAdapterResponseInternationalPayout(
				CREATECONTRACTFSFSRES createContractFSResp) throws Exception {
			InternationalPayoutResponseDomainDTO internationalPayoutResponse = new InternationalPayoutResponseDomainDTO();
			if (createContractFSResp.getFCUBSHEADER().getMSGSTAT()
					.equals(com.ofss.fcubs124.service.fcubsftservice.MsgStatType.SUCCESS)) {
				internationalPayoutResponse
						.setHostReference(createContractFSResp.getFCUBSBODY().getContractDetailsFull().getCONTREFNO());
			}

			return internationalPayoutResponse;
		}



}
