package com.ecobank.digx.cz.extxface.payment.ubs124.impl.transfer.assembler;

import com.ecobank.digx.cz.extxface.fc12if.adapter.ubs124.dto.BatchPostingRequestDTO;
import com.ecobank.digx.cz.extxface.fc12if.adapter.ubs124.dto.TransactionEntry;
import com.ofss.digx.app.payment.dto.BillPaymentRequestDomainDTO;
import com.ofss.digx.app.payment.dto.BillPaymentResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.PeerToPeerRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.PeerToPeerResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.externalTransfer.ExternalTransferDTO;
import com.ofss.digx.app.payment.dto.transfer.externalTransfer.ExternalTransferResponse;
import com.ofss.digx.datatype.CurrencyAmount;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.digx.domain.account.entity.core.AccountKey;
import com.ofss.digx.domain.dda.entity.DemandDepositAccount;
import com.ofss.digx.enumeration.payment.payee.PayeeType;
import com.ofss.digx.extxface.ubs124.impl.AbstractAdapterHelper;
import com.ofss.digx.extxface.ubs124.impl.PaymentAdapterHelper;
import com.ofss.digx.extxface.ubs124.impl.RequestHeader;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSREQ;
import com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSRES;
import com.ofss.fcubs.service.fcubsifservice.ExtAccEcaEntriesFullType;
import com.ofss.fcubs124.service.fcubscustomerservice.AmtBlkFullType;
import com.ofss.fcubs124.service.fcubscustomerservice.CREATEAMTBLKFSFSREQ;
import com.ofss.fcubs124.service.fcubscustomerservice.CREATEAMTBLKFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.CREATEPCCONTRACTFSFSREQ;
import com.ofss.fcubs124.service.fcubsextpcservice.CREATEPCCONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.PcdtronlFullType;
import com.ofss.fcubs124.service.fcubsextpcservice.UBSCOMPType;
import com.ofss.fcubs124.service.fcubsftservice.CREATECONTRACTFSFSREQ;
import com.ofss.fcubs124.service.fcubsftservice.ContractFullType;
import com.ofss.fcubs124.service.fcubsftservice.TxnSetlDtlsCreateIOType.ContractSettlement;
import com.ofss.fcubs124.service.fcubsftservice.TxnSetlDtlsFullType;
import com.ofss.fcubs124.service.fcubsupservice.CREATEUPTRANSACTIONFSFSREQ;
import com.ofss.fcubs124.service.fcubsupservice.CREATEUPTRANSACTIONFSFSRES;
import com.ofss.fcubs124.service.fcubsupservice.FCUBSHEADERType;
import com.ofss.fcubs124.service.fcubsupservice.MsgStatType;
import com.ofss.fcubs124.service.fcubsupservice.UtilPaymentTxnFullType;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

public class PaymentTransferAssembler {
	private static final String THIS_COMPONENT_NAME = PaymentTransferAssembler.class.getName();
	private static final MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);

	public CREATEEXTACCECAENTRIESFSFSREQ fromAdaptertoHostRequestSelf(SelfTransferRequestDomainDTO selfTransferReqDTO)
			throws Exception {
		AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
		PaymentAdapterHelper paymentAdapterHelper = PaymentAdapterHelper.getInstance();
		RequestHeader header = helper.getRequestHeader(null, "FCUBSIFServiceSEI", "createExtAccEcaEntriesFS");
		CREATEEXTACCECAENTRIESFSFSREQ createPCContractRequest = new CREATEEXTACCECAENTRIESFSFSREQ();
		com.ofss.fcubs.service.fcubsifservice.FCUBSHEADERType fcUBSHeaderType = new com.ofss.fcubs.service.fcubsifservice.FCUBSHEADERType();
		fcUBSHeaderType.setUSERID(header.getUserid());
		fcUBSHeaderType.setCORRELID(selfTransferReqDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setMSGID(selfTransferReqDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setBRANCH(helper.getProcessingBranch() != null
				? helper.getProcessingBranch()
				: helper.getHostBranchId(selfTransferReqDTO.getDebitAccountId()));
		fcUBSHeaderType.setUBSCOMP(com.ofss.fcubs.service.fcubsifservice.UBSCOMPType.FCUBS);
		fcUBSHeaderType.setSERVICE(header.getService());
		fcUBSHeaderType.setOPERATION(header.getOperation());
		fcUBSHeaderType.setMSGSTAT(com.ofss.fcubs.service.fcubsifservice.MsgStatType.SUCCESS);
		fcUBSHeaderType.setSOURCE(header.getSource());
		createPCContractRequest.setFCUBSHEADER(fcUBSHeaderType);
		
		CREATEEXTACCECAENTRIESFSFSREQ.FCUBSBODY fcUBSBody = new CREATEEXTACCECAENTRIESFSFSREQ.FCUBSBODY();
		
		PcdtronlFullType pcdtronlFullType = new PcdtronlFullType();
		ExtAccEcaEntriesFullType accEntryMasterFull = new ExtAccEcaEntriesFullType();
		
		accEntryMasterFull.setEVENT("INIT");
	    accEntryMasterFull.setGRPREFNO(selfTransferReqDTO.getSystemReferenceNumber());
	    accEntryMasterFull.setSOURCECODE("FCAT");
	    accEntryMasterFull.setUNIQUEEXTREFNO(selfTransferReqDTO.getSystemReferenceNumber());
	    accEntryMasterFull.setTXNBRN(helper.getHostBranchId(selfTransferReqDTO.getDebitAccountId()));
		
		
	    List<ExtAccEcaEntriesFullType.AccEntryDetails> list = new ArrayList<>();
	      ExtAccEcaEntriesFullType.AccEntryDetails t = null;

	        t = new ExtAccEcaEntriesFullType.AccEntryDetails();
	        t.setACBRANCH(helper.getHostBranchId(helper.getHostBranchId(selfTransferReqDTO.getDebitAccountId())));
	        t.setACCCY(paymentAdapterHelper.fetchAccountCurrency(
					helper.getHostAccountId(selfTransferReqDTO.getDebitAccountId()),
					helper.getHostBranchId(selfTransferReqDTO.getDebitAccountId())));
	        t.setACNO(helper.getHostAccountId(selfTransferReqDTO.getDebitAccountId()));
	        t.setEVENT("INIT");
	        /*t.setINSTRUMENTCODE(te.getInstrumentNo());
	        //t.setDRCRIND(te.getCreditOrDebit());
	        //t.setEXCHRATE(te.getExchangeRate());
	        //t.setEVENTSRNO(new BigDecimal(te.getSerialNo().intValue()));
	       // t.setRELATEDCUSTOMER(selfTransferReqDTO.getPartyId());
	        //t.setTRNREFNO(selfTransferReqDTO.getSystemReferenceNumber());
	        //t.setVALUEDT(helper.getPaymentHostDate(selfTransferReqDTO.getPaymentDate()));
	        t.setMODULE("IF");
	        t.setLCYAMT(selfTransferReqDTO.getPmtAmount().getAmount());
	          if (!lcyCcy.equals(te.getCurrencyCode()))
	       //     t.setFCYAMT(te.getAmount()); 
	         
	        //t.setTXNNARRATIVE(selfTransferReqDTO.getRemarks());
	        //t.setTRNCODE(te.getTransactionCode());
	        t.setDONTSHOWINSTMT("N");*/
	        t.setAVAILBALREQD("Y"); 
	        list.add(t);
	      
	    
	    
	    
		pcdtronlFullType.setSRCCOD("FCAT");
		pcdtronlFullType.setSRCREF(selfTransferReqDTO.getSystemReferenceNumber());
		pcdtronlFullType.setTXNCCY(paymentAdapterHelper.fetchAccountCurrency(
				helper.getHostAccountId(selfTransferReqDTO.getCreditAccountId()),
				helper.getHostBranchId(selfTransferReqDTO.getCreditAccountId())));
		pcdtronlFullType.setCUSACCCY(paymentAdapterHelper.fetchAccountCurrency(
				helper.getHostAccountId(selfTransferReqDTO.getDebitAccountId()),
				helper.getHostBranchId(selfTransferReqDTO.getDebitAccountId())));
		pcdtronlFullType.setACTAMT(selfTransferReqDTO.getPmtAmount().getAmount());
		pcdtronlFullType.setREMRK(selfTransferReqDTO.getRemarks());
		pcdtronlFullType.setCPACNO(helper.getHostAccountId(selfTransferReqDTO.getCreditAccountId()));
		pcdtronlFullType.setACBRN1(helper.getHostBranchId(selfTransferReqDTO.getDebitAccountId()));
		pcdtronlFullType.setCUSACNO(helper.getHostAccountId(selfTransferReqDTO.getDebitAccountId()));
		pcdtronlFullType.setCPBNKCOD(
				paymentAdapterHelper.fetchBankCode(helper.getHostBranchId(selfTransferReqDTO.getDebitAccountId())));
		pcdtronlFullType.setCOUNTERPRTYNAME(helper.getAdapterImplConfiguration("SELFFT", "SELFNICKNAME"));
		if (selfTransferReqDTO.getDealId() != null) {
			pcdtronlFullType.setFXCONTRACTREF(selfTransferReqDTO.getDealId());
			pcdtronlFullType.setPRDCOD(helper.getAdapterImplConfiguration("SELFFT_FX", "PRODCOD"));
			pcdtronlFullType.setPRDTYP("O");
			pcdtronlFullType.setCUSTNO(selfTransferReqDTO.getPartyId());
			pcdtronlFullType.setPRDCAT(helper.getAdapterImplConfiguration("SELFFT_FX", "PRODCAT"));
		} else {
			pcdtronlFullType.setPRDCOD(helper.getAdapterImplConfiguration("SELFFT", "PRODCOD"));
			pcdtronlFullType.setPRDCAT(helper.getAdapterImplConfiguration("SELFFT", "PRODCAT"));
		}
		try {
			pcdtronlFullType.setACTDT(helper.getPaymentHostDate(selfTransferReqDTO.getPaymentDate()));
		} catch (DatatypeConfigurationException e) {
			logger.log(Level.SEVERE, formatter.formatMessage(
					" Exception has occured while getting response object of %s inside the fromAdaptertoHostRequestSelf method of %s for %s. Exception details are %s",
					new Object[]{SelfTransferRequestDomainDTO.class.getName(), THIS_COMPONENT_NAME, selfTransferReqDTO,
							e}),
					e);
		}
		pcdtronlFullType.setAUTHORIZATIONSTATUS("A");
		//fcUBSBody.setPctbContractMasterFull(pcdtronlFullType);
		createPCContractRequest.setFCUBSBODY(fcUBSBody);
		return createPCContractRequest;
	}

	public SelfTransferResponseDomainDTO fromHosttoAdapterResponseSelf(CREATEEXTACCECAENTRIESFSFSRES createIFContractFS)
			throws Exception {
		SelfTransferResponseDomainDTO selfTransferResponse = new SelfTransferResponseDomainDTO();
		if (createIFContractFS.getFCUBSHEADER().getMSGSTAT()
				.equals((Object) com.ofss.fcubs.service.fcubsifservice.MsgStatType.SUCCESS)) {
			selfTransferResponse.setHostReference(
					createIFContractFS.getFCUBSBODY().getAccEntryMasterFull().getGRPREFNO().toString());
		}
		return selfTransferResponse;
	}

	public CREATEPCCONTRACTFSFSREQ fromAdaptertoHostRequestp2pSender(PeerToPeerRequestDomainDTO p2pTransferDTO)
			throws Exception {
		AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
		PaymentAdapterHelper paymentAdapterHelper = PaymentAdapterHelper.getInstance();
		CREATEPCCONTRACTFSFSREQ createPCContractRequest = new CREATEPCCONTRACTFSFSREQ();
		com.ofss.fcubs124.service.fcubsextpcservice.FCUBSHEADERType fcUBSHeaderType = new com.ofss.fcubs124.service.fcubsextpcservice.FCUBSHEADERType();
		CREATEPCCONTRACTFSFSREQ.FCUBSBODY fcUBSBody = new CREATEPCCONTRACTFSFSREQ.FCUBSBODY();
		PcdtronlFullType pcdtronlFullType = new PcdtronlFullType();
		fcUBSHeaderType.setUBSCOMP(UBSCOMPType.FCUBS);
		fcUBSHeaderType.setMSGID(p2pTransferDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setCORRELID(p2pTransferDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setUSERID("FCATOP");
		fcUBSHeaderType.setBRANCH(helper.getProcessingBranch() != null
				? helper.getProcessingBranch()
				: AbstractAdapterHelper.getInstance().getHostBranchId(p2pTransferDTO.getDebitAccountId()));
		fcUBSHeaderType.setSERVICE("FCUBSExtPCService");
		fcUBSHeaderType.setOPERATION("CreatePCContract");
		fcUBSHeaderType.setSOURCEOPERATION("CreatePCContract");
		fcUBSHeaderType.setMSGSTAT(com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS);
		fcUBSHeaderType.setSOURCE("FCAT");
		createPCContractRequest.setFCUBSHEADER(fcUBSHeaderType);
		pcdtronlFullType.setTXNCCY(p2pTransferDTO.getPmtAmount().getCurrency());
		pcdtronlFullType.setCUSACCCY(
				paymentAdapterHelper.fetchAccountCurrency(helper.getHostAccountId(p2pTransferDTO.getDebitAccountId()),
						helper.getHostBranchId(p2pTransferDTO.getDebitAccountId())));
		pcdtronlFullType.setREMRK(p2pTransferDTO.getRemarks());
		pcdtronlFullType.setSRCCOD("FCAT");
		pcdtronlFullType.setSRCREF(p2pTransferDTO.getSystemReferenceNumber());
		pcdtronlFullType.setPRDTYP("O");
		pcdtronlFullType.setCUSTNO(p2pTransferDTO.getPartyId());
		pcdtronlFullType
				.setACBRN1(AbstractAdapterHelper.getInstance().getHostBranchId(p2pTransferDTO.getDebitAccountId()));
		pcdtronlFullType
				.setCUSACNO(AbstractAdapterHelper.getInstance().getHostAccountId(p2pTransferDTO.getDebitAccountId()));
		pcdtronlFullType.setFCYAMT(p2pTransferDTO.getPmtAmount().getAmount());
		pcdtronlFullType.setTXNAMT(p2pTransferDTO.getPmtAmount().getAmount());
		pcdtronlFullType.setCPBNKCOD("");
		pcdtronlFullType.setCPACNO("");
		pcdtronlFullType.setPRDCOD(helper.getAdapterImplConfiguration("PEERTOPEER", "PRODCOD"));
		pcdtronlFullType.setPRDCAT(helper.getAdapterImplConfiguration("PEERTOPEER", "PRODCAT"));
		pcdtronlFullType.setCOUNTERPRTYNAME(p2pTransferDTO.getPayeeNickname());
		try {
			pcdtronlFullType.setACTDT(helper.getPaymentHostDate(
					paymentAdapterHelper.fetchValueDate(helper.getHostBranchId(p2pTransferDTO.getDebitAccountId()))));
		} catch (DatatypeConfigurationException e) {
			logger.log(Level.SEVERE, formatter.formatMessage(
					" Exception has occured while getting response object of %s inside the fromAdaptertoHostRequestp2pSender method of %s for %s. Exception details are %s",
					new Object[]{PeerToPeerRequestDomainDTO.class.getName(), THIS_COMPONENT_NAME, p2pTransferDTO, e}),
					e);
		}
		pcdtronlFullType.setACTAMT(p2pTransferDTO.getPmtAmount().getAmount());
		pcdtronlFullType.setCUSINF1(p2pTransferDTO.getPartyId());
		pcdtronlFullType.setCHGBRER("");
		pcdtronlFullType.setREDISREQ("Y");
		pcdtronlFullType.setAUTHORIZATIONSTATUS("A");
		if (p2pTransferDTO.getContactDetails().equals("EMAIL")) {
			pcdtronlFullType.setEMAILID(p2pTransferDTO.getTransferValue());
		} else if (p2pTransferDTO.getContactDetails().equals("MOBILE")) {
			pcdtronlFullType.setCPINF3(p2pTransferDTO.getTransferValue());
		} else {
			pcdtronlFullType.setFACEBKID(p2pTransferDTO.getTransferValue());
		}
		pcdtronlFullType.setLASTEVENTCODE("0");
		pcdtronlFullType.setCUSBNKCOD("");
		pcdtronlFullType.setCONTRACTREFERENCENUMBER("");
		try {
			pcdtronlFullType.setCLGBRN(paymentAdapterHelper.getClearingBranchId(
					AbstractAdapterHelper.getInstance().getHostBranchId(p2pTransferDTO.getDebitAccountId())));
		} catch (java.lang.Exception e) {
			logger.log(Level.SEVERE, formatter.formatMessage(
					" Exception has occured while getting response object of %s inside the fromAdaptertoHostRequestp2pSender method of %s for %s. Exception details are %s",
					new Object[]{PeerToPeerRequestDomainDTO.class.getName(), THIS_COMPONENT_NAME, p2pTransferDTO}), e);
		}
		fcUBSBody.setPctbContractMasterFull(pcdtronlFullType);
		createPCContractRequest.setFCUBSBODY(fcUBSBody);
		return createPCContractRequest;
	}

	public CREATEAMTBLKFSFSREQ fromAdaptertoHostRequestp2pSenderOtherwise(PeerToPeerRequestDomainDTO p2pTransferDTO)
			throws Exception {
		AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
		CREATEAMTBLKFSFSREQ createamtblkfsfsreq = new CREATEAMTBLKFSFSREQ();
		com.ofss.fcubs124.service.fcubscustomerservice.FCUBSHEADERType header = new com.ofss.fcubs124.service.fcubscustomerservice.FCUBSHEADERType();
		header.setSOURCE("FCAT");
		header.setUBSCOMP(com.ofss.fcubs124.service.fcubscustomerservice.UBSCOMPType.FCUBS);
		header.setMSGID(p2pTransferDTO.getSystemReferenceNumber());
		header.setCORRELID(p2pTransferDTO.getSystemReferenceNumber());
		header.setUSERID("FCATOP");
		header.setBRANCH(helper.getProcessingBranch() != null
				? helper.getProcessingBranch()
				: AbstractAdapterHelper.getInstance().getHostBranchId(p2pTransferDTO.getDebitAccountId()));
		header.setSERVICE("FCUBSCustomerService");
		header.setOPERATION("CreateAmtBlk");
		header.setSOURCEOPERATION("CreateAmtBlk");
		header.setMSGSTAT(com.ofss.fcubs124.service.fcubscustomerservice.MsgStatType.SUCCESS);
		createamtblkfsfsreq.setFCUBSHEADER(header);
		CREATEAMTBLKFSFSREQ.FCUBSBODY body = new CREATEAMTBLKFSFSREQ.FCUBSBODY();
		AmtBlkFullType type = new AmtBlkFullType();
		try {
			if (p2pTransferDTO.getEffectiveDate() != null) {
				type.setEFFDATE(helper.getPaymentHostDate(p2pTransferDTO.getEffectiveDate()));
				type.setEXPDATE(helper.getPaymentHostDate(p2pTransferDTO.getExpiryDate()));
			}
		} catch (DatatypeConfigurationException e) {
			logger.log(Level.SEVERE, formatter.formatMessage(
					" Exception has occured while getting response object of %s inside the fromAdaptertoHostRequestp2pSenderOtherwise method of %s for %s. Exception details are %s",
					new Object[]{PeerToPeerRequestDomainDTO.class.getName(), THIS_COMPONENT_NAME, p2pTransferDTO, e}),
					e);
		}
		type.setACC(AbstractAdapterHelper.getInstance().getHostAccountId(p2pTransferDTO.getDebitAccountId()));
		type.setAMTBLKNO("");
		type.setAMT(p2pTransferDTO.getPmtAmount().getAmount());
		type.setREM("");
		type.setABLKTYPE("I");
		type.setREFERENCENO(p2pTransferDTO.getSystemReferenceNumber());
		type.setHPCODE("");
		type.setHOLDDESC("");
		type.setBRANCH(AbstractAdapterHelper.getInstance().getHostBranchId(p2pTransferDTO.getDebitAccountId()));
		if (p2pTransferDTO.getContactDetails().equals("EMAIL")) {
			type.setBENEFICIARYEMAILID(p2pTransferDTO.getTransferValue());
		}
		if (p2pTransferDTO.getContactDetails().equals("MOBILE")) {
			type.setBENEFICIARYTELEPHONE(p2pTransferDTO.getTransferValue());
		}
		type.setMODNO(BigDecimal.valueOf(0.0));
		type.setTXNSTAT("");
		type.setAUTHSTAT("A");
		body.setAmountBlocksFull(type);
		createamtblkfsfsreq.setFCUBSBODY(body);
		return createamtblkfsfsreq;
	}

	public PeerToPeerResponseDomainDTO fromHosttoAdapterResponsep2pSender(CREATEPCCONTRACTFSFSRES createPCContractFS)
			throws Exception {
		PeerToPeerResponseDomainDTO responseDTO = new PeerToPeerResponseDomainDTO();
		if (createPCContractFS.getFCUBSHEADER().getMSGSTAT()
				.equals((Object) com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS)) {
			responseDTO.setHostReference(
					createPCContractFS.getFCUBSBODY().getPctbContractMasterFull().getCONTRACTREFERENCENUMBER());
		}
		return responseDTO;
	}

	public CREATEPCCONTRACTFSFSREQ fromAdaptertoHostRequestp2pReceiver(PeerToPeerRequestDomainDTO p2pTransferDTO)
			throws Exception {
		AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
		CREATEPCCONTRACTFSFSREQ createPCContractRequest = new CREATEPCCONTRACTFSFSREQ();
		PaymentAdapterHelper paymentAdapterHelper = PaymentAdapterHelper.getInstance();
		com.ofss.fcubs124.service.fcubsextpcservice.FCUBSHEADERType fcUBSHeaderType = new com.ofss.fcubs124.service.fcubsextpcservice.FCUBSHEADERType();
		CREATEPCCONTRACTFSFSREQ.FCUBSBODY fcUBSBody = new CREATEPCCONTRACTFSFSREQ.FCUBSBODY();
		PcdtronlFullType pcdtronlFullType = new PcdtronlFullType();
		fcUBSHeaderType.setUBSCOMP(UBSCOMPType.FCUBS);
		fcUBSHeaderType.setMSGID(p2pTransferDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setCORRELID(p2pTransferDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setUSERID("FCATOP");
		fcUBSHeaderType.setBRANCH(helper.getProcessingBranch() != null
				? helper.getProcessingBranch()
				: AbstractAdapterHelper.getInstance().getHostBranchId(p2pTransferDTO.getDebitAccountId()));
		fcUBSHeaderType.setSERVICE("FCUBSExtPCService");
		fcUBSHeaderType.setOPERATION("CreatePCContract");
		fcUBSHeaderType.setSOURCEOPERATION("CreatePCContract");
		fcUBSHeaderType.setMSGSTAT(com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS);
		fcUBSHeaderType.setSOURCE("FCAT");
		createPCContractRequest.setFCUBSHEADER(fcUBSHeaderType);
		pcdtronlFullType.setTXNCCY(p2pTransferDTO.getPmtAmount().getCurrency());
		pcdtronlFullType.setCUSACCCY(
				paymentAdapterHelper.fetchAccountCurrency(helper.getHostAccountId(p2pTransferDTO.getDebitAccountId()),
						helper.getHostBranchId(p2pTransferDTO.getDebitAccountId())));
		pcdtronlFullType.setREMRK(p2pTransferDTO.getRemarks());
		try {
			pcdtronlFullType.setCLGBRN(paymentAdapterHelper.getClearingBranchId(
					AbstractAdapterHelper.getInstance().getHostBranchId(p2pTransferDTO.getDebitAccountId())));
		} catch (java.lang.Exception e) {
			logger.log(Level.SEVERE, formatter.formatMessage(
					" Exception has occured while getting response object of %s inside the fromAdaptertoHostRequestp2pSender method of %s for %s. Exception details are %s",
					new Object[]{PeerToPeerRequestDomainDTO.class.getName(), THIS_COMPONENT_NAME, p2pTransferDTO}), e);
		}
		pcdtronlFullType.setSRCCOD("FCAT");
		pcdtronlFullType.setSRCREF(p2pTransferDTO.getSystemReferenceNumber());
		pcdtronlFullType.setPRDTYP("O");
		pcdtronlFullType.setCUSTNO(p2pTransferDTO.getPartyId());
		pcdtronlFullType
				.setACBRN1(AbstractAdapterHelper.getInstance().getHostBranchId(p2pTransferDTO.getDebitAccountId()));
		pcdtronlFullType
				.setCUSACNO(AbstractAdapterHelper.getInstance().getHostAccountId(p2pTransferDTO.getDebitAccountId()));
		pcdtronlFullType.setFCYAMT(p2pTransferDTO.getPmtAmount().getAmount());
		pcdtronlFullType.setTXNAMT(p2pTransferDTO.getPmtAmount().getAmount());
		if (p2pTransferDTO.getCreditGLOrHold().equals("G")) {
			pcdtronlFullType.setCUSTOMERDEBITREF(p2pTransferDTO.getHostReferenceNo());
		} else {
			pcdtronlFullType.setAMOUNTBLOCKREF(p2pTransferDTO.getHostReferenceNo());
		}
		pcdtronlFullType.setCUSBNKCOD("");
		if (p2pTransferDTO.getAccountType().equals((Object) PayeeType.INTERNAL)) {
			pcdtronlFullType.setCPBNKCOD(
					paymentAdapterHelper.fetchBankCode(helper.getHostBranchId(p2pTransferDTO.getCreditAccountId())));
		}
		if (p2pTransferDTO.getAccountType().equals((Object) PayeeType.DOMESTIC)) {
			pcdtronlFullType.setCPBNKCOD(p2pTransferDTO.getCodBank());
		}
		pcdtronlFullType
				.setCPACNO(AbstractAdapterHelper.getInstance().getHostAccountId(p2pTransferDTO.getCreditAccountId()));
		if (null != p2pTransferDTO.getAccountName()) {
			pcdtronlFullType.setCOUNTERPRTYNAME(p2pTransferDTO.getAccountName());
		} else {
			pcdtronlFullType.setCOUNTERPRTYNAME(p2pTransferDTO.getUserName());
		}
		if (p2pTransferDTO.getAccountType().equals((Object) PayeeType.INTERNAL)) {
			pcdtronlFullType.setPRDCOD(helper.getAdapterImplConfiguration("PEERTOPEER_RCV_INT", "PRODCOD"));
			pcdtronlFullType.setPRDCAT(helper.getAdapterImplConfiguration("PEERTOPEER_RCV_INT", "PRODCAT"));
		}
		if (p2pTransferDTO.getAccountType().equals((Object) PayeeType.DOMESTIC)) {
			pcdtronlFullType.setPRDCOD(helper.getAdapterImplConfiguration("PEERTOPEER_RCV_EXT", "PRODCOD"));
			pcdtronlFullType.setPRDCAT(helper.getAdapterImplConfiguration("PEERTOPEER_RCV_EXT", "PRODCAT"));
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String vDate = df.format(
				paymentAdapterHelper.fetchPostingDate(helper.getHostBranchId(p2pTransferDTO.getDebitAccountId())));
		pcdtronlFullType.setACTDT(paymentAdapterHelper.toXMLGregorianCalendar(vDate));
		if (p2pTransferDTO.getPmtAmount() != null) {
			pcdtronlFullType.setACTAMT(p2pTransferDTO.getPmtAmount().getAmount());
		}
		pcdtronlFullType.setCUSINF1(p2pTransferDTO.getPartyId());
		pcdtronlFullType.setCHGBRER("");
		pcdtronlFullType.setREDISREQ("Y");
		pcdtronlFullType.setLASTEVENTCODE("0");
		pcdtronlFullType.setAUTHORIZATIONSTATUS("A");
		if (p2pTransferDTO.getContactDetails().equals("EMAIL")) {
			pcdtronlFullType.setEMAILID(p2pTransferDTO.getTransferValue());
		}
		if (p2pTransferDTO.getContactDetails().equals("MOBILE")) {
			pcdtronlFullType.setCPINF3(p2pTransferDTO.getTransferValue());
		}
		pcdtronlFullType.setNETWRK(p2pTransferDTO.getNetworktype());
		fcUBSBody.setPctbContractMasterFull(pcdtronlFullType);
		createPCContractRequest.setFCUBSBODY(fcUBSBody);
		return createPCContractRequest;
	}

	public PeerToPeerResponseDomainDTO fromHosttoAdapterResponsep2pReceiver(CREATEPCCONTRACTFSFSRES createPCContractFS)
			throws Exception {
		PeerToPeerResponseDomainDTO responseDTO = new PeerToPeerResponseDomainDTO();
		if (createPCContractFS.getFCUBSHEADER().getMSGSTAT()
				.equals((Object) com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS)) {
			responseDTO.setHostReference(
					createPCContractFS.getFCUBSBODY().getPctbContractMasterFull().getCONTRACTREFERENCENUMBER());
		}
		return responseDTO;
	}

	public PeerToPeerResponseDomainDTO fromHosttoAdapterResponsep2pReceiverOtherwise(CREATEAMTBLKFSFSRES response)
			throws Exception {
		PeerToPeerResponseDomainDTO responseDTO = new PeerToPeerResponseDomainDTO();
		if (response.getFCUBSHEADER().getMSGSTAT()
				.toString() == com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS.toString()) {
			responseDTO.setHostReference(response.getFCUBSBODY().getAmountBlocksFull().getAMTBLKNO());
		}
		return responseDTO;
	}

	public CREATEPCCONTRACTFSFSREQ fromAdaptertoHostRequestInternal(
			InternalTransferRequestDomainDTO internalTransferReqDTO) throws Exception {
		AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
		PaymentAdapterHelper paymentAdapterHelper = PaymentAdapterHelper.getInstance();
		RequestHeader header = helper.getRequestHeader(null, "FCUBSExtPCService", "CreatePCContract");
		CREATEPCCONTRACTFSFSREQ createPCContractRequest = new CREATEPCCONTRACTFSFSREQ();
		com.ofss.fcubs124.service.fcubsextpcservice.FCUBSHEADERType fcUBSHeaderType = new com.ofss.fcubs124.service.fcubsextpcservice.FCUBSHEADERType();
		fcUBSHeaderType.setUSERID(header.getUserid());
		fcUBSHeaderType.setCORRELID(internalTransferReqDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setMSGID(internalTransferReqDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setBRANCH(helper.getProcessingBranch() != null
				? helper.getProcessingBranch()
				: helper.getHostBranchId(internalTransferReqDTO.getDebitAccountId()));
		fcUBSHeaderType.setUBSCOMP(UBSCOMPType.FCUBS);
		fcUBSHeaderType.setSERVICE(header.getService());
		fcUBSHeaderType.setOPERATION(header.getOperation());
		fcUBSHeaderType.setMSGSTAT(com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS);
		fcUBSHeaderType.setSOURCE(header.getSource());
		createPCContractRequest.setFCUBSHEADER(fcUBSHeaderType);
		CREATEPCCONTRACTFSFSREQ.FCUBSBODY fcUBSBody = new CREATEPCCONTRACTFSFSREQ.FCUBSBODY();
		PcdtronlFullType pcdtronlFullType = new PcdtronlFullType();
		pcdtronlFullType.setSRCCOD("FCAT");
		pcdtronlFullType.setSRCREF(internalTransferReqDTO.getSystemReferenceNumber());
		pcdtronlFullType.setTXNCCY(internalTransferReqDTO.getPmtAmount().getCurrency());
		pcdtronlFullType.setCUSACCCY(paymentAdapterHelper.fetchAccountCurrency(
				helper.getHostAccountId(internalTransferReqDTO.getDebitAccountId()),
				helper.getHostBranchId(internalTransferReqDTO.getDebitAccountId())));
		pcdtronlFullType.setACTAMT(internalTransferReqDTO.getPmtAmount().getAmount());
		pcdtronlFullType.setREMRK(internalTransferReqDTO.getRemarks());
		pcdtronlFullType.setCPACNO(helper.getHostAccountId(internalTransferReqDTO.getCreditAccountId()));
		pcdtronlFullType.setACBRN1(helper.getHostBranchId(internalTransferReqDTO.getDebitAccountId()));
		pcdtronlFullType.setCUSACNO(helper.getHostAccountId(internalTransferReqDTO.getDebitAccountId()));
		pcdtronlFullType.setCPBNKCOD(
				paymentAdapterHelper.fetchBankCode(helper.getHostBranchId(internalTransferReqDTO.getDebitAccountId())));
		pcdtronlFullType.setCOUNTERPRTYNAME(internalTransferReqDTO.getAccountName());
		try {
			pcdtronlFullType.setACTDT(helper.getPaymentHostDate(internalTransferReqDTO.getPaymentDate()));
		} catch (DatatypeConfigurationException e) {
			logger.log(Level.SEVERE, formatter.formatMessage(
					" Exception has occured while getting response object of %s inside the fromAdaptertoHostRequestInternal method of %s for %s. Exception details are %s",
					new Object[]{InternalTransferRequestDomainDTO.class.getName(), THIS_COMPONENT_NAME,
							internalTransferReqDTO}),
					e);
		}
		pcdtronlFullType.setAUTHORIZATIONSTATUS("A");
		pcdtronlFullType.setCATGPURPTYP("C");
		String[] purposeDetails = paymentAdapterHelper.fetchPurpose(internalTransferReqDTO.getPurpose());
		if (purposeDetails != null) {
			pcdtronlFullType.setCATEGORYPURPOSE(purposeDetails[0]);
			if (null != purposeDetails[0] && purposeDetails[0].equalsIgnoreCase("OTH")) {
				pcdtronlFullType.setPAYDET1(purposeDetails[1]);
			}
		}
		if (internalTransferReqDTO.getDealId() != null) {
			pcdtronlFullType.setFXCONTRACTREF(internalTransferReqDTO.getDealId());
			pcdtronlFullType.setPRDCOD(helper.getAdapterImplConfiguration("INTERNALFT_FX", "PRODCOD"));
			pcdtronlFullType.setPRDTYP("O");
			pcdtronlFullType.setCUSTNO(internalTransferReqDTO.getPartyId());
			pcdtronlFullType.setPRDCAT(helper.getAdapterImplConfiguration("INTERNALFT_FX", "PRODCAT"));
		} else {
			pcdtronlFullType.setPRDCOD(helper.getAdapterImplConfiguration("INTERNALFT", "PRODCOD"));
			pcdtronlFullType.setPRDCAT(helper.getAdapterImplConfiguration("INTERNALFT", "PRODCAT"));
		}
		fcUBSBody.setPctbContractMasterFull(pcdtronlFullType);
		createPCContractRequest.setFCUBSBODY(fcUBSBody);
		return createPCContractRequest;
	}

	public InternalTransferResponseDomainDTO fromHosttoAdapterResponseInternal(
			CREATEPCCONTRACTFSFSRES createPCContractFS) throws Exception {
		InternalTransferResponseDomainDTO internalTransferResponse = new InternalTransferResponseDomainDTO();
		if (createPCContractFS.getFCUBSHEADER().getMSGSTAT()
				.equals((Object) com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS)) {
			internalTransferResponse.setHostReference(
					createPCContractFS.getFCUBSBODY().getPctbContractMasterFull().getCONTRACTREFERENCENUMBER());
		}
		return internalTransferResponse;
	}

	public CREATEPCCONTRACTFSFSREQ fromAdaptertoHostRequestExternalTransfer(ExternalTransferDTO externalTransferDTO)
			throws Exception {
		AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
		PaymentAdapterHelper paymentAdapterHelper = PaymentAdapterHelper.getInstance();
		RequestHeader header = helper.getRequestHeader(null, "FCUBSExtPCService", "CreatePCContract");
		CREATEPCCONTRACTFSFSREQ createPCContractRequest = new CREATEPCCONTRACTFSFSREQ();
		com.ofss.fcubs124.service.fcubsextpcservice.FCUBSHEADERType fcUBSHeaderType = new com.ofss.fcubs124.service.fcubsextpcservice.FCUBSHEADERType();
		fcUBSHeaderType.setSOURCE(header.getSource());
		fcUBSHeaderType.setUBSCOMP(UBSCOMPType.FCUBS);
		fcUBSHeaderType.setMSGID(externalTransferDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setCORRELID(externalTransferDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setUSERID(header.getUserid());
		fcUBSHeaderType.setBRANCH(helper.getProcessingBranch() != null
				? helper.getProcessingBranch()
				: helper.getHostBranchId(externalTransferDTO.getDebitAccountId().getValue().toString()));
		fcUBSHeaderType.setSERVICE(header.getService());
		fcUBSHeaderType.setOPERATION(header.getOperation());
		fcUBSHeaderType.setSOURCEOPERATION("");
		createPCContractRequest.setFCUBSHEADER(fcUBSHeaderType);
		CREATEPCCONTRACTFSFSREQ.FCUBSBODY fcUBSBody = new CREATEPCCONTRACTFSFSREQ.FCUBSBODY();
		PcdtronlFullType pcdtronlFullType = new PcdtronlFullType();
		pcdtronlFullType.setSRCCOD("FCAT");
		pcdtronlFullType.setSRCREF(externalTransferDTO.getSystemReferenceNumber());
		pcdtronlFullType.setPRDCAT(helper.getAdapterImplConfiguration("EXTERNALFT", "PRODCAT"));
		pcdtronlFullType.setPRDCOD(helper.getAdapterImplConfiguration("EXTERNALFT", "PRODCOD"));
		pcdtronlFullType.setTXNCCY(externalTransferDTO.getAmount().getCurrency());
		pcdtronlFullType.setCUSACCCY(paymentAdapterHelper.fetchAccountCurrency(
				helper.getHostAccountId(externalTransferDTO.getDebitAccountId().getValue().toString()),
				helper.getHostBranchId(externalTransferDTO.getDebitAccountId().getValue().toString())));
		pcdtronlFullType.setACTAMT(externalTransferDTO.getAmount().getAmount());
		pcdtronlFullType.setREMRK(externalTransferDTO.getRemarks());
		pcdtronlFullType.setCPACNO(helper.getHostAccountId(externalTransferDTO.getCreditAccountId()));
		pcdtronlFullType
				.setACBRN1(helper.getHostBranchId(externalTransferDTO.getDebitAccountId().getValue().toString()));
		pcdtronlFullType
				.setCUSACNO(helper.getHostAccountId(externalTransferDTO.getDebitAccountId().getValue().toString()));
		pcdtronlFullType.setCPBNKCOD(paymentAdapterHelper
				.fetchBankCode(helper.getHostBranchId(externalTransferDTO.getDebitAccountId().getValue().toString())));
		try {
			pcdtronlFullType.setACTDT(helper.getPaymentHostDate(paymentAdapterHelper.fetchValueDate(
					helper.getHostBranchId(externalTransferDTO.getDebitAccountId().getValue().toString()))));
		} catch (DatatypeConfigurationException e) {
			logger.log(Level.SEVERE, formatter.formatMessage(
					" Exception has occured while getting response object of %s inside the fromAdaptertoHostRequestExternalTransfer method of %s for %s. Exception details are %s",
					new Object[]{ExternalTransferDTO.class.getName(), THIS_COMPONENT_NAME, externalTransferDTO, e}), e);
		}
		pcdtronlFullType.setAUTHORIZATIONSTATUS("A");
		pcdtronlFullType.setPURPOSETYPE("C");
		String[] purposeDetails = paymentAdapterHelper.fetchPurpose(externalTransferDTO.getPurpose());
		if (purposeDetails != null) {
			pcdtronlFullType.setCATEGORYPURPOSE(purposeDetails[0]);
			if (null != purposeDetails[0] && purposeDetails[0].equalsIgnoreCase("OTH")) {
				pcdtronlFullType.setPAYDET1(purposeDetails[1]);
			}
		}
		fcUBSBody.setPctbContractMasterFull(pcdtronlFullType);
		createPCContractRequest.setFCUBSBODY(fcUBSBody);
		return createPCContractRequest;
	}

	public ExternalTransferResponse fromHosttoAdapterResponseExternalTransfer(
			CREATEPCCONTRACTFSFSRES createPCContractFS) throws Exception {
		ExternalTransferResponse externalTransferResponse = new ExternalTransferResponse();
		if (createPCContractFS.getFCUBSHEADER().getMSGSTAT()
				.equals((Object) com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS)) {
			externalTransferResponse.setExternalReferenceId(
					createPCContractFS.getFCUBSBODY().getPctbContractMasterFull().getCONTRACTREFERENCENUMBER());
		}
		return externalTransferResponse;
	}

	public CREATEUPTRANSACTIONFSFSREQ fromAdaptertoHostBillPayment(
			BillPaymentRequestDomainDTO billPaymentRequestDomainDTO) throws Exception {
		AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
		PaymentAdapterHelper paymentAdapterHelper = PaymentAdapterHelper.getInstance();
		RequestHeader header = helper.getRequestHeader(null, "FCUBSUPService", "CreateUPTransaction");
		CREATEUPTRANSACTIONFSFSREQ createuptransactionfsfsreq = new CREATEUPTRANSACTIONFSFSREQ();
		FCUBSHEADERType fcUBSHeaderType = new FCUBSHEADERType();
		CREATEUPTRANSACTIONFSFSREQ.FCUBSBODY fcUBSBody = new CREATEUPTRANSACTIONFSFSREQ.FCUBSBODY();
		fcUBSHeaderType.setMSGID(billPaymentRequestDomainDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setCORRELID(billPaymentRequestDomainDTO.getSystemReferenceNumber());
		fcUBSHeaderType.setBRANCH(helper.getProcessingBranch() != null
				? helper.getProcessingBranch()
				: AbstractAdapterHelper.getInstance().getHostBranchId(billPaymentRequestDomainDTO.getDebitAccountId()));
		fcUBSHeaderType.setSOURCEOPERATION(header.getOperation());
		fcUBSHeaderType.setUSERID(header.getUserid());
		fcUBSHeaderType.setSERVICE(header.getService());
		fcUBSHeaderType.setOPERATION(header.getOperation());
		fcUBSHeaderType.setSOURCE(header.getSource());
		fcUBSHeaderType.setDESTINATION(header.getDestination());
		fcUBSHeaderType.setMODULEID("UP");
		fcUBSHeaderType.setSOURCEUSERID("");
		fcUBSHeaderType.setUBSCOMP(com.ofss.fcubs124.service.fcubsupservice.UBSCOMPType.FCUBS);
		createuptransactionfsfsreq.setFCUBSHEADER(fcUBSHeaderType);
		UtilPaymentTxnFullType type = new UtilPaymentTxnFullType();
		type.setPRD(helper.getAdapterImplConfiguration("BILLPAYMENT", "PRODCOD"));
		type.setBILLNO(billPaymentRequestDomainDTO.getBillReferenceNumber());
		type.setBAMT(billPaymentRequestDomainDTO.getPmtAmount().getAmount());
		type.setCUSTACNO(
				AbstractAdapterHelper.getInstance().getHostAccountId(billPaymentRequestDomainDTO.getDebitAccountId()));
		type.setXREF(billPaymentRequestDomainDTO.getSystemReferenceNumber());
		type.setNARRATIVE(billPaymentRequestDomainDTO.getRemarks());
		try {
			type.setTXNDATE((Object) helper.getPaymentHostDate(paymentAdapterHelper
					.fetchValueDate(helper.getHostBranchId(billPaymentRequestDomainDTO.getDebitAccountId()))));
			type.setBILLDT((Object) helper.getPaymentHostDate(billPaymentRequestDomainDTO.getBillDate()));
		} catch (DatatypeConfigurationException e) {
			logger.log(Level.SEVERE, formatter.formatMessage(
					" Exception has occured while getting response object of %s inside the fromAdaptertoHostBillPayment method of %s for %s. Exception details are %s",
					new Object[]{BillPaymentRequestDomainDTO.class.getName(), THIS_COMPONENT_NAME,
							billPaymentRequestDomainDTO}),
					e);
		}
		type.setINSTID(billPaymentRequestDomainDTO.getIdBiller());
		type.setBCCY(billPaymentRequestDomainDTO.getPmtAmount().getCurrency());
		type.setTXNCCY(billPaymentRequestDomainDTO.getPmtAmount().getCurrency());
		type.setTXNBRN(
				AbstractAdapterHelper.getInstance().getHostBranchId(billPaymentRequestDomainDTO.getDebitAccountId()));
		type.setACCCY(paymentAdapterHelper.fetchAccountCurrency(
				helper.getHostAccountId(billPaymentRequestDomainDTO.getDebitAccountId()),
				helper.getHostBranchId(billPaymentRequestDomainDTO.getDebitAccountId())));
		type.setCONSNO(billPaymentRequestDomainDTO.getBillerAccountNumber());
		fcUBSBody.setTransactionDetails(type);
		createuptransactionfsfsreq.setFCUBSBODY(fcUBSBody);
		return createuptransactionfsfsreq;
	}

	public BillPaymentResponseDomainDTO fromHosttoAdapterBillPayment(
			CREATEUPTRANSACTIONFSFSRES createuptransactionfsfsres) throws Exception {
		BillPaymentResponseDomainDTO responseDTO = new BillPaymentResponseDomainDTO();
		if (createuptransactionfsfsres.getFCUBSHEADER().getMSGSTAT().equals((Object) MsgStatType.SUCCESS)) {
			responseDTO.setHostReference(createuptransactionfsfsres.getFCUBSBODY().getTransactionDetails().getFCCREF());
		}
		return responseDTO;
	}
	
	
	
	  private BatchPostingRequestDTO fromSingleTransactionRequestToBatchPosting(SelfTransferRequestDomainDTO request) throws Exception {
		    BatchPostingRequestDTO batchRequest = new BatchPostingRequestDTO();
		  //  String affCode = request.getAffiliateCode();
		    AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
		    PaymentAdapterHelper paymentAdapterHelper = PaymentAdapterHelper.getInstance();
		    return batchRequest;
		  }
		    
		    

		  
	
	
}