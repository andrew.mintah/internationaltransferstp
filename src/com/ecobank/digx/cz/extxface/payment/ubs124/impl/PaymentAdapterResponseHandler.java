package com.ecobank.digx.cz.extxface.payment.ubs124.impl;

import com.ofss.digx.extxface.message.Message;
import com.ofss.digx.extxface.response.AbstractResponseHandler;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSRES;
import com.ofss.fcubs124.service.fcubscustomerservice.CREATEAMTBLKFSFSRES;
import com.ofss.fcubs124.service.fcubsddservice.CREATETRANSACTIONFSFSRES;
import com.ofss.fcubs124.service.fcubsddservice.ERRORDETAILSType;
import com.ofss.fcubs124.service.fcubsddservice.ERRORType;
import com.ofss.fcubs124.service.fcubsddservice.MsgStatType;
import com.ofss.fcubs124.service.fcubsextpcservice.CLOSEPCMAINTFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.CREATEPCCONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.CREATEPCMAINTFSFSRES;
import com.ofss.fcubs124.service.fcubsextpcservice.REVERSEPCCONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsftservice.CLOSECONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsftservice.CREATECONTRACTFSFSRES;
//import com.ofss.fcubs124.service.fcubspcservice.CREATEPCCONTRACTFSFSRES;
//import com.ofss.fcubs124.service.fcubssiservice.CLOSECONTRACTFSFSRES;
import com.ofss.fcubs124.service.fcubsupservice.CREATEUPTRANSACTIONFSFSRES;
import com.ofss.fcubs124.service.fcubsupservice.FCUBSHEADERType;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PaymentAdapterResponseHandler extends AbstractResponseHandler {
	private static final String THIS_COMPONENT_NAME = PaymentAdapterResponseHandler.class.getName();
	private static final MultiEntityLogger FORMATTER = MultiEntityLogger.getUniqueInstance();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	private static final String MESSAGE_SEPARATOR = "~";

	public void checkResponseProcessSelfTransfer(CREATEEXTACCECAENTRIESFSFSRES createIfContractResponse) throws Exception {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					FORMATTER.formatMessage(
							"Entered in method checkResponseProcessSelfTransfer of %s, hostResponse = %s",
							new Object[]{THIS_COMPONENT_NAME, createIfContractResponse}));
		}
		if (createIfContractResponse == null) {
			this.analyseResponse("DIGX_PROD_DEF_0000", this.getClass());
		} else if (!createIfContractResponse.getFCUBSHEADER().getMSGSTAT()
				.equals((Object) com.ofss.fcubs.service.fcubsifservice.MsgStatType.SUCCESS)
				&& createIfContractResponse.getFCUBSBODY().getFCUBSERRORRESP() != null
				&& createIfContractResponse.getFCUBSBODY().getFCUBSERRORRESP().size() > 0) {
			ArrayList<Message> messages = new ArrayList<Message>();
			for (com.ofss.fcubs.service.fcubsifservice.ERRORType FCUBSERRORRESP : createIfContractResponse
					.getFCUBSBODY().getFCUBSERRORRESP()) {
				for (com.ofss.fcubs.service.fcubsifservice.ERRORDETAILSType error : FCUBSERRORRESP.getERROR()) {
					messages.add(new Message(error.getECODE()));
				}
			}
			this.analyseResponse("PAYMENT", messages, this.getClass(), "DIGX_PY_101");
		}
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Exiting from method checkResponseProcessSelfTransfer of %s, createIfContractResponse is = %s",
					new Object[]{THIS_COMPONENT_NAME, createIfContractResponse}));
		}
	}

	public void checkResponseProcessp2pTransfer(CREATEPCCONTRACTFSFSRES createpccontractfsfsres) throws Exception {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					FORMATTER.formatMessage(
							"Entered in method checkResponseProcessp2pTransfer of %s, hostResponse = %s",
							new Object[]{THIS_COMPONENT_NAME, createpccontractfsfsres}));
		}
		if (createpccontractfsfsres == null) {
			this.analyseResponse("DIGX_PROD_DEF_0000", this.getClass());
		} else if (!createpccontractfsfsres.getFCUBSHEADER().getMSGSTAT()
				.equals((Object) com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS)
				&& createpccontractfsfsres.getFCUBSBODY().getFCUBSERRORRESP() != null
				&& createpccontractfsfsres.getFCUBSBODY().getFCUBSERRORRESP().length > 0) {
			ArrayList<Message> messages = new ArrayList<Message>();
			for (com.ofss.fcubs124.service.fcubsextpcservice.ERRORType FCUBSERRORRESP : createpccontractfsfsres
					.getFCUBSBODY().getFCUBSERRORRESP()) {
				for (com.ofss.fcubs124.service.fcubsextpcservice.ERRORDETAILSType error : FCUBSERRORRESP.getERROR()) {
					messages.add(new Message(error.getECODE()));
				}
			}
			this.analyseResponse("PAYMENT", messages, this.getClass(), "DIGX_PY_130");
		}
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Exiting from method checkResponseProcessp2pTransfer of %s, createIfContractResponse is = %s",
					new Object[]{THIS_COMPONENT_NAME, createpccontractfsfsres}));
		}
	}

	public void checkResponseProcessSelfTransferSI(CREATEPCMAINTFSFSRES createpcmaintfsfsres) throws Exception {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					FORMATTER.formatMessage(
							"Entered in method checkResponseProcessSelfTransferSI of %s, hostResponse = %s",
							new Object[]{THIS_COMPONENT_NAME, createpcmaintfsfsres}));
		}
		if (createpcmaintfsfsres == null) {
			this.analyseResponse("DIGX_PROD_DEF_0000", this.getClass());
		} else if (!createpcmaintfsfsres.getFCUBSHEADER().getMSGSTAT()
				.equals((Object) com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS)
				&& createpcmaintfsfsres.getFCUBSBODY().getFCUBSERRORRESP() != null
				&& createpcmaintfsfsres.getFCUBSBODY().getFCUBSERRORRESP().length > 0) {
			ArrayList<Message> messages = new ArrayList<Message>();
			for (com.ofss.fcubs124.service.fcubsextpcservice.ERRORType FCUBSERRORRESP : createpcmaintfsfsres
					.getFCUBSBODY().getFCUBSERRORRESP()) {
				for (com.ofss.fcubs124.service.fcubsextpcservice.ERRORDETAILSType error : FCUBSERRORRESP.getERROR()) {
					messages.add(new Message(error.getECODE()));
				}
			}
			this.analyseResponse("PAYMENT", messages, this.getClass(), "DIGX_PY_103");
		}
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Exiting from method checkResponseProcessSelfTransferSI of %s, createIfContractResponse is = %s",
					new Object[]{THIS_COMPONENT_NAME, createpcmaintfsfsres}));
		}
	}

	public void checkResponseProcessInternalTransferSI(CREATEPCMAINTFSFSRES createpcmaintfsfsres) throws Exception {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					FORMATTER.formatMessage(
							"Entered in method checkResponseProcessInternalTransferSI of %s, hostResponse = %s",
							new Object[]{THIS_COMPONENT_NAME, createpcmaintfsfsres}));
		}
		if (createpcmaintfsfsres == null) {
			this.analyseResponse("DIGX_PROD_DEF_0000", this.getClass());
		} else if (!createpcmaintfsfsres.getFCUBSHEADER().getMSGSTAT()
				.equals((Object) com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS)
				&& createpcmaintfsfsres.getFCUBSBODY().getFCUBSERRORRESP() != null
				&& createpcmaintfsfsres.getFCUBSBODY().getFCUBSERRORRESP().length > 0) {
			ArrayList<Message> messages = new ArrayList<Message>();
			for (com.ofss.fcubs124.service.fcubsextpcservice.ERRORType FCUBSERRORRESP : createpcmaintfsfsres
					.getFCUBSBODY().getFCUBSERRORRESP()) {
				for (com.ofss.fcubs124.service.fcubsextpcservice.ERRORDETAILSType error : FCUBSERRORRESP.getERROR()) {
					messages.add(new Message(error.getECODE()));
				}
			}
			this.analyseResponse("PAYMENT", messages, this.getClass(), "DIGX_PY_104");
		}
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Exiting from method checkResponseProcessInternalTransferSI of %s, createIfContractResponse is = %s",
					new Object[]{THIS_COMPONENT_NAME, createpcmaintfsfsres}));
		}
	}

	public void checkResponseProcessInternalTransfer(CREATEPCCONTRACTFSFSRES createIfContractResponse)
			throws Exception {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Entered in method checkResponseProcessInternalTransfer of %s, createIfContractResponse = %s",
					new Object[]{THIS_COMPONENT_NAME, createIfContractResponse}));
		}
		if (createIfContractResponse == null) {
			this.analyseResponse("DIGX_PROD_DEF_0000", this.getClass());
		} else if (!createIfContractResponse.getFCUBSHEADER().getMSGSTAT()
				.equals((Object) com.ofss.fcubs124.service.fcubsextpcservice.MsgStatType.SUCCESS)
				&& createIfContractResponse.getFCUBSBODY().getFCUBSERRORRESP() != null
				&& createIfContractResponse.getFCUBSBODY().getFCUBSERRORRESP().length > 0) {
			ArrayList<Message> messages = new ArrayList<Message>();
			for (com.ofss.fcubs124.service.fcubsextpcservice.ERRORType FCUBSERRORRESP : createIfContractResponse
					.getFCUBSBODY().getFCUBSERRORRESP()) {
				for (com.ofss.fcubs124.service.fcubsextpcservice.ERRORDETAILSType error : FCUBSERRORRESP.getERROR()) {
					messages.add(new Message(error.getECODE()));
				}
			}
			this.analyseResponse("PAYMENT", messages, this.getClass(), "DIGX_PY_102");
		}
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Exiting from method checkResponseProcessInternalTransfer of %s, createIfContractResponse is = %s",
					new Object[]{THIS_COMPONENT_NAME, createIfContractResponse}));
		}
	}

	
	public void checkResponseProcessInternationalPayout(CREATECONTRACTFSFSRES createContractFSResp) throws Exception {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Entered in method checkResponseProcessInternationalPayout of %s, createContractFSResp = %s",
					new Object[]{THIS_COMPONENT_NAME, createContractFSResp}));
		}

		if (createContractFSResp == null) {
			this.analyseResponse("DIGX_PROD_DEF_0000", this.getClass());
		} else if (!createContractFSResp.getFCUBSHEADER().getMSGSTAT().equals(MsgStatType.SUCCESS)
				&& createContractFSResp.getFCUBSBODY().getFCUBSERRORRESP() != null
				&& createContractFSResp.getFCUBSBODY().getFCUBSERRORRESP().length > 0) {
			List<Message> messages = new ArrayList();
			com.ofss.fcubs124.service.fcubsftservice.ERRORType[] var3 = createContractFSResp.getFCUBSBODY()
					.getFCUBSERRORRESP();
			int var4 = var3.length;

			for (int var5 = 0; var5 < var4; ++var5) {
				com.ofss.fcubs124.service.fcubsftservice.ERRORType FCUBSERRORRESP = var3[var5];
				com.ofss.fcubs124.service.fcubsftservice.ERRORDETAILSType[] var7 = FCUBSERRORRESP.getERROR();
				int var8 = var7.length;

				for (int var9 = 0; var9 < var8; ++var9) {
					com.ofss.fcubs124.service.fcubsftservice.ERRORDETAILSType error = var7[var9];
					messages.add(new Message(error.getECODE()));
				}
			}

			this.analyseResponse("PAYMENT", messages, this.getClass(), "DIGX_PY_109");
		}

	}
}