package com.ecobank.digx.cz.extxface.payment.adapter;

import com.ofss.digx.app.maintenance.dto.MaintenanceRequestDTO;
import com.ofss.digx.app.maintenance.dto.MaintenanceResponseDTO;
import com.ofss.digx.app.payment.dto.BillPaymentRequestDomainDTO;
import com.ofss.digx.app.payment.dto.BillPaymentResponseDomainDTO;
import com.ofss.digx.app.payment.dto.CancelInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.CancelInstructionResponseDomainDTO;
import com.ofss.digx.app.payment.dto.PaymentDetailsDTO;
import com.ofss.digx.app.payment.dto.ViewTransactionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.draft.DomesticDemandDraftRequestDomainDTO;
import com.ofss.digx.app.payment.dto.draft.DomesticDemandDraftResponseDomainDTO;
import com.ofss.digx.app.payment.dto.draft.InternationalDraftRequestDomainDTO;
import com.ofss.digx.app.payment.dto.draft.InternationalDraftResponseDomainDTO;
import com.ofss.digx.app.payment.dto.instruction.payin.USDomesticPayinInstructionDomainDTO;
import com.ofss.digx.app.payment.dto.payout.CancellationOfFutureDatedTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticPayoutInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticPayoutRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticPayoutResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCardPayoutRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCardPayoutResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCreditTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaCreditTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaDirectDebitRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticSepaDirectDebitResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUKPaymentsInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUKPaymentsRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUKPaymentsResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUSPaymentRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.DomesticUSPaymentResponseDomainDTO;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutCreateRequestDTO;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutRequestDomainDTO;
import com.ofss.digx.app.payment.dto.payout.InternationalPayoutResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.CancellationOfFutureDatedTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.DomesticSepaCardPayoutInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.DomesticSepaCreditTransferInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.DomesticSepaDirectDebitInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.InternalTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.PeerToPeerRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.PeerToPeerResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferInstructionRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferRequestDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.SelfTransferResponseDomainDTO;
import com.ofss.digx.app.payment.dto.transfer.externalTransfer.ExternalTransferDTO;
import com.ofss.digx.app.payment.dto.transfer.externalTransfer.ExternalTransferResponse;
import com.ofss.digx.enumeration.payment.instructions.PayinResponseType;
import com.ofss.digx.infra.exceptions.Exception;
import java.util.List;

public interface IPaymentAdapter {
	public SelfTransferResponseDomainDTO processSelfTransfer(SelfTransferRequestDomainDTO var1) throws Exception;

	public InternalTransferResponseDomainDTO processInternalTransfer(InternalTransferRequestDomainDTO var1)
			throws Exception;

	public SelfTransferResponseDomainDTO processSelfTransferInstruction(SelfTransferInstructionRequestDomainDTO var1)
			throws Exception;

	public InternalTransferResponseDomainDTO processInternalAccountTransferInstruction(
			InternalTransferInstructionRequestDomainDTO var1) throws Exception;

	public InternationalPayoutResponseDomainDTO processInternationalTransfer(InternationalPayoutRequestDomainDTO var1)
			throws Exception;
	
}


